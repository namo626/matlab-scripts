function total_uniform(Nm1, Nm2, Amp1, Amp2)

for magNum = Nm1:2:Nm2
  define_parameters(0.025, magNum, 32, 2);
  for forcing = Amp1:0.1:Amp2
    flow_evolution(forcing, 300, 1/32);
%     save_omega(sprintf('%dC_%4.3f.mat', magNum, forcing));
    plot_omega(sprintf('%dC_%4.3f.mat', magNum, forcing));
    % save animation
    
  end
end


end
