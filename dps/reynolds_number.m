function [Re_t, Re_avg, Re_unc] = reynolds_number(filename)
% Accepts a matfile containing recorded flow field, calculates the Re at
% each timestep (snapshot), and outputs them as a row vector.

% OUTPUT
% Re_t = Re at every timestep
% Re_avg = average Re
% Re_unc = uncertainty in average Re

load(filename, 'u_t', 'v_t', 'nu', 'L0', 'Nx', 'Ny', 'amp')

% Re = (u * L) / nu
transient = 50;
t = size(u_t, 3);
Re_t = zeros(1, t);
V = Re_t;

for i = 1:t
  rmsVel = sqrt((sum(sum(u_t(:,:,i).^2 + v_t(:,:,i).^2))) / (Nx * Ny));
  V(i) = rmsVel;
  Re_t(i) = (L0 * rmsVel) / nu;

end

V_unc = std(V);
V_avg = mean(V(transient:end));
Re_avg = (L0 * V_avg) / nu;
Re_unc = Re_avg * sqrt((V_unc/V_avg)^2);

% %plotRe;
%
%   function plotRe
%     plot(1:t, reNorm)
%     xlim([0, t])
%     xlabel('t', 'FontSize', 14)
%     ylabel('Re', 'FontSize', 14)
%     title(sprintf('Reynold''s number, Nm = %d, Forcing = %.1f, L0 = %.3f, nu = %.2e', Nm, amp, L0, nu));
%   end

end
