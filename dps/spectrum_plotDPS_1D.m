function spectrum_plotDPS_1D(k, ex, k_leftx, k_rightx,ey,k_lefty,k_righty,plotTitle)
% spectrum_plot: plots mean energy spectrum of flow snapshots after
% transient. Shows curve fits for inverse casecade range and casecade
% range

% This plots both kx and ky

k_forcing = 125.7;
k_x = 2*pi/(4*0.025);
k_y = 2*pi/(32*0.025);
tolerance = 0.01;
transient = 1;
ind_forcing = find(abs(k-k_forcing) < tolerance);

%% Kx
% for kx
lim_leftx = find(abs(k-k_leftx) < tolerance);
lim_rightx = find(abs(k-k_rightx) < tolerance);
mean_energyx = mean(ex(transient:end, :));

scatter(k, mean_energyx, 100, 'filled');
hold on

% fitting the kx inverse cascade range
k_invx = k(lim_leftx:ind_forcing);
lk_invx = log(k_invx);
le_invx = log(mean_energyx(lim_leftx:ind_forcing));
pfit_invx = polyfit(lk_invx(isfinite(le_invx)), le_invx(isfinite(le_invx)), 1);
% plot the curve fit
plot(k_invx, k_invx.^pfit_invx(1)*exp(1)^pfit_invx(2), 'LineWidth', 2);


% fitting the cascade range
% for kx
k_casx = k(ind_forcing:lim_rightx);
lk_casx = log(k_casx);
le_casx = log(mean_energyx(ind_forcing:lim_rightx));
pfit_casx = polyfit(lk_casx(isfinite(le_casx)), le_casx(isfinite(le_casx)), 1);
% plot the curve fit
plot(k_casx, k_casx.^pfit_casx(1)*exp(1)^pfit_casx(2), 'LineWidth', 2);



%% Ky

% for ky
lim_lefty = find(abs(k-k_lefty) < tolerance);
lim_righty = find(abs(k-k_righty) < tolerance);
mean_energyy = mean(ey(transient:end, :));

scatter(k, mean_energyy, 100, '^','filled');

% for ky
k_invy = k(lim_lefty:ind_forcing);
lk_invy = log(k_invy);
le_invy = log(mean_energyy(lim_lefty:ind_forcing));
pfit_invy = polyfit(lk_invy(isfinite(le_invy)), le_invy(isfinite(le_invy)), 1);
% plot the curve fit
plot(k_invy, k_invy.^pfit_invy(1)*exp(1)^pfit_invy(2), 'LineWidth', 2);

% for ky
k_casy = k(ind_forcing:lim_righty);
lk_casy = log(k_casy);
le_casy = log(mean_energyy(ind_forcing:lim_righty));
pfit_casy = polyfit(lk_casy(isfinite(le_casy)), le_casy(isfinite(le_casy)), 1);
% plot the curve fit
plot(k_casy, k_casy.^pfit_casy(1)*exp(1)^pfit_casy(2), 'LineWidth', 2);

%% Cleaning up overall plot

title(plotTitle, 'FontSize', 13)
xlabel('wavenumber')
ylabel('Energy')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

xlim([10^0 10^4])
ylim([10^-4, 10^4])
yl = ylim;

% plot forcing wavenumber
plot([k_forcing, k_forcing], yl,'-','LineWidth', 1)
% plot k for horizontal box size
plot([k_x, k_x], yl,'--','LineWidth', 1)
% plot k for vertical box size (min k)
plot([k_y, k_y], yl,'--', 'LineWidth', 1)


legend_casx = sprintf('k^{%.2f}', pfit_casx(1));
legend_invx = sprintf('k^{%.2f}', pfit_invx(1));
legend_casy = sprintf('k^{%.2f}', pfit_casy(1));
legend_invy = sprintf('k^{%.2f}', pfit_invy(1));
legend({'kx', legend_invx, legend_casx, ...
        'ky', legend_invy, legend_casy, ...
        'forcing','Box_x','Box_y'}, ...
        'FontSize',14)



end
