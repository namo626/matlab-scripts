function subplot_omega(file_name1, file_name2)
load(file_name1,'omega_t','u_t','v_t','X','Y','t_stamps','amp','Nm','L0');
omega_t1 = omega_t;
amp1 = amp;
X1 = X;
Y1 = Y;

load(file_name2,'omega_t','u_t','v_t','X','Y','t_stamps','amp','Nm','L0');

for i = 1:size(omega_t,3) %#ok<NODEF>
    subplot(211);
%     clf
    colormap jet
%     imagesc(omega_t(:,:,i))
    h = pcolor(X1,Y1,omega_t1(:,:,i));
    set(h,'EdgeColor','none')
    hold on
%     h = quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),u_t(1:10:end,1:10:end,i),v_t(1:10:end,1:10:end,i));
%     set(h,'Color','k')
    title(['A = ' num2str(amp1) ' , t = ' sprintf('%1.2f',t_stamps(i)) ' s']);
    hold off
    
    subplot(212);
    
    colormap jet
%     imagesc(omega_t(:,:,i))
    h = pcolor(X,Y,omega_t(:,:,i));
    set(h,'EdgeColor','none')
    hold on
%     h = quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),u_t(1:10:end,1:10:end,i),v_t(1:10:end,1:10:end,i));
%     set(h,'Color','k')
    title(['A = ' num2str(amp) ' , t = ' sprintf('%1.2f',t_stamps(i)) ' s']);
    
    pause(0.01)
end
% imagesc(mean(omega_t,3));

