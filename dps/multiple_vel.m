% script to plot spatial velocity profile of different alphas (DPS)

range = [0 1 3];
A = cell(length(range), 3);
% loading date from different alphas
k = 1;
% l = [4.0 5.8 8.0];
l = [0.6 0.7 0.8 0.9];
for i = 1:length(l)
    filename = sprintf('32x4C_%.1f00_0a_500s.mat', l(k));
    C = load(filename, ...
            'u_t',...
            'dim', 'amp','alpha');
    y = size(C.('u_t'), 1);
%     t = size(C.('u_t'), 3);
    t = 501;
    dim = C.('dim');
    amp = C.('amp');
    
    u_t = C.('u_t');
    u_avg = zeros(t, y);
    
    for j = 1:t
        u_avg(j, :) = mean(u_t(:,:,j), 2);
    end
    
    % Reynold's number
    [~, Re_avg, ~] = reynolds_number(filename);
    
    A{k, 1} = u_avg;
    A{k, 2} = C.('alpha');
    A{k, 3} = round(Re_avg);
    
    clear C

    k = k + 1;
end
        


% allocating avg arrays for each alpha data
% for i = range
% u_avg = zeros(t, y);
% v_avg = zeros(t, y);
% 
% for i = 1:t
%     for j = 1:5
%         u_t = A
%   u_avg(i, :) = mean(u_t(:,:,i), 2);
%   v_avg(i, :) = mean(v_t(:,:,i), 2);
%     end

% for i = 1:5 % for each box
%     u_t = A{i, 1}.('u_t');
%     u_avg = zeros(t, y);
%     for j = 1:t
%         u_avg(j, :) = mean(u_t(:,:,j), 2);
%     end
%     A{i, 2} = u_avg;
% end
%  
winSize = 10;
v = VideoWriter(sprintf('%sC_treshold_0a.avi', dim));
v.FrameRate = 15;
open(v);

% loops through time
for j = winSize:t
  clf
  lowerBound = j - winSize + 1;
  % loops through each box
  for b = 1:length(l)
    switch b
        case 1
            s = '-';
        case 2
            s = '-';
        case 3
            s = '--';
        otherwise
            s = ':';
    end
    u_avg = A{b, 1};  
    alpha = A{b, 2};
    if b == 1
        w = 3;
    else
        w = 2;
    end
    plot(1:y, mean(u_avg(lowerBound:j,:)), s, 'LineWidth', w);
   
    hold on
  end
  legend({sprintf('F = 0.6, Re = %d', A{1, 3}),...
      sprintf('F = 0.7, Re = %d', A{2, 3}),...
      sprintf('F = 0.8, Re = %d', A{3, 3}),...
      sprintf('F = 0.9, Re = %d', A{4, 3})},...
      'FontSize', 13);
  
%   plot(1:y, v_avg(j,:));

  xlabel('Y')

  title(sprintf('(DPS) Nm = %s, alpha = 0, t = %d-%ds', ...
    dim, lowerBound, j));
  ylabel('Spatial avg of velocity')
  ylim([-0.07, 0.07])

  frame = getframe(gcf);
  writeVideo(v, frame);
  pause(0.001);
end
close(v);


    

