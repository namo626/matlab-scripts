function [spatial_mean] = spatial_plot(filename)
% plots the variation of average vorticitiy field over time

load(filename, 'omega_t', 'amp', 'Nm', 'ind', 'setup', 'Nx', 'tf');

% init = 66;
% om = omega_t(:,:,init:end);
t = size(omega_t, 3);
spatial_mean = zeros(1, t);


for i = 1:t
  snapshot = omega_t(:,:,i);
  spatial_mean(i) = mean(snapshot(:));
end

plot(1:t, spatial_mean);
title(sprintf('(Spatial) Average vorticity field over time, Nm = %d', Nm));
xlabel('t', 'FontSize', 13)
ylabel('(Spatial) Average vorticity field', 'FontSize', 13)
 

end

