function trimmed = trimFourier(fourier)
  trimmed = fourier(:,1:end/2+1);
  [r, c] = size(fourier);
  
  for i = 1:r
    P = abs(fourier(i,:)/c);
    P = P(1:c/2+1);
    P(2:end-1) = 2*P(2:end-1);
    trimmed(i,:) = P;
  end
  
end
