function correlation_plot(filename)
load(filename, 'amp','setup','ind','Nm');

rec = correlation_get(filename);

if setup == 'C'
  mode = 'Sinusoidal Checkerboard';
  
elseif setup == 'S'
  mode = 'Sinusoidal Checkerboard';
  
else
  mode = 'Randomized Checkerboard';
  
end

t = 0:length(rec)-1;

plot(t, rec);
xlabel('t', 'FontSize', 15);
ylabel('$ \Vert \Omega(t)-\Omega(0) \Vert $', 'Interpreter', 'latex', 'FontSize', 15);
xlim([0,max(t)]);
title(sprintf('Correlation, %s #%d, Nm = %d, amp = %.3f', mode, ind, Nm, amp));

% save image
text = [num2str(Nm), setup, num2str(ind), '_', num2str(amp) '_corr'];
print([text '.png'], '-dpng','-r300');

end
  