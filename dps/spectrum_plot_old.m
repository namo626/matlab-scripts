function spectrum_plot(k, energy, transient, k_left, k_right, plotTitle)
% spectrum_plot: plots mean energy spectrum of flow snapshots after
% transient. Shows curve fits for inverse casecade range and casecade
% range

% *** This function is obsolete, since we know plot kx and ky separately

k_forcing = 178;
ind_forcing = find(k==k_forcing);
lim_left = find(k==k_left);
lim_right = find(k==k_right);
% transient = 50; % this is overestimated, just for safety
mean_energy = mean(energy(transient:end, :));
% plot the overall spectrum
scatter(k, mean_energy, 5, 'filled');
title(plotTitle, 'FontSize', 13)
xlabel('wavenumber')
ylabel('Energy')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
hold on

% fitting the inverse cascade range
k_inv = k(lim_left:ind_forcing);
e_inv = mean_energy(lim_left:ind_forcing);
pfit_inv = polyfit(log(k_inv), log(e_inv), 1);
% plot the curve fit
plot(k_inv, k_inv.^pfit_inv(1)*exp(1)^pfit_inv(2), 'LineWidth', 2);

% fitting the cascade range
k_cas = k(ind_forcing:lim_right);
e_cas = mean_energy(ind_forcing:lim_right);
pfit_cas = polyfit(log(k_cas), log(e_cas), 1);
% plot the curve fit
plot(k_cas, k_cas.^pfit_cas(1)*exp(1)^pfit_cas(2), 'LineWidth', 2);


yl = ylim;
% plot forcing wavenumber
plot([k_forcing, k_forcing], yl)

legend_cas = sprintf('k^{%.2f}', pfit_cas(1));
legend_inv = sprintf('k^{%.2f}', pfit_inv(1));
legend({'data', legend_inv, legend_cas, 'forcing'}, 'FontSize',14)



end
