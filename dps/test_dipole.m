gdp = 10;
M = zeros(gdp,gdp);
y0 = 1;
center = (gdp+1)/2;
cen = 0.5;
for i = 1:gdp
  for j = 1:gdp
    r = sqrt(y0^2 + (0.1*i-cen)^2 + (0.1*j-cen)^2);
    M(i,j) = M(i,j) + (3-r^2)/r^5;
  end
end
