function spectrum_plotter(k, ex, ey, plotTitle)
% This plots both kx and ky
% DPS

% Adjust these parameters according to domain size
% Assuming magnet width is 0.025
k_forcing = 125.7;
k_x = 2*pi/(4*0.025);
k_y = 2*pi/(28*0.025);
transient = 100;


mean_ex = mean(ex(transient:end,:));
mean_ey = mean(ey(transient:end,:));


% plot the overall spectrum
scatter(k, mean_ex, 80, 'filled');
hold on
scatter(k, mean_ey, 80, '^','filled');
title(plotTitle, 'FontSize', 13)
xlabel('wavenumber')
ylabel('Energy')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

ylim([10^-4, 10^4])
yl = ylim;
% plot forcing wavenumber
plot([k_forcing, k_forcing], yl,'LineWidth', 2)
% plot k for horizontal box size
plot([k_x, k_x], yl,'--','LineWidth', 1)
% plot k for vertical box size (min k)
plot([k_y, k_y], yl, '--','LineWidth', 1)


xlim([10^(0) 10^5])

legend({'kx','ky','forcing','horizontal box size','vertical box size'}, ...
        'FontSize', 15)



end
