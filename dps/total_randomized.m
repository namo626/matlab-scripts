function total_randomized(Nm1, Nm2, Amp1, Amp2)

for magNum = Nm1:2:Nm2
  for i = 1:3
    define_parameters(0.025, magNum, 32, 3);
    for forcing = Amp1:0.1:Amp2
      flow_evolution(forcing, 300, 1/32, i-1);
      plot_omega(sprintf('%dR%d_%4.3f.mat', magNum, i-1, forcing));
      save_omega(sprintf('%dR%d_%4.3f.mat', magNum, i-1, forcing));
      
    end
  end
end

end