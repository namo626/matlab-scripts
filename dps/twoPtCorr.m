function corr = twoPtCorr(linearSnapshots, p1, p2)
  %Xavg = mean(linearSnapshots);
  X = linearSnapshots;
  corr = X(:, p1) .* X(:, p2) ./ mean(X(:, p1).^2);
  corr = mean(corr);
  
end
    
    