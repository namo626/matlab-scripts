%--------------------------flow_evolution.m-------------------------------
% Written: Radford Mitchell
% Editted: Logan Kageorge
%-------------------------------------------------------------------------
% This function evolves the flow according to the parameters set in 
% define_parameters. The linear and nonlinear operators are split using 
% the Strang-Marchuk protocol so that evolutions in the Fourier domain can
% be stepped forward using appropriate schemes. amp is the forcing
% amplitude, tf is the final integration time, dt is the time step.
%
% Example: flow_evolution(0.15, 300, 1/32)
%-------------------------------------------------------------------------
%cfl condition
%correlation plot (3T)
%average spatially (flash red and blue)
%% The Time-Stepper
function flow_evolution(amp,tf,dt,ind)
% i is the number of test -Namo

% initialize the parameters as globals to access them in the integrator
global nu alpha F D2 ID2 Dx Dy %#ok<*NUSED>

% loads simulation parameters
load('parameters.mat');

% Initialize forcing, time and the vorticity
F  = amp * F0;
t0 = 0;
t  = 0;

omega = 0.2.*rand(Ny,Nx);
omega0 = omega;   % initialize some random vorticity field
%load(filename,'omega0')
%omega = omega0 + 0.0001*rand(Ny,Nx);
Omega = fftshift(fft2(omega));     % get the initial field in Fourier space
Psi   = -ID2.*Omega;               % get the stream function
U     = Dy.*Psi;                   % get the x_velocity in Fourier space
V     = -Dx.*Psi;                  % get the y_velocity in Fourier space
u     = real(ifft2(ifftshift(U))); % get the x_velocity in physical space
v     = real(ifft2(ifftshift(V))); % get the x_velocity in physical space

sv_num   = 1;          % counts the number of saves
dt_sv    = 1;          % how often we save the vorticity
t_stamps = 0:dt_sv:tf; % time stamps for the saved fields
t_sv     = t0 + dt_sv; % step the save timestamp

omega_t = zeros([size(D2),length(t_stamps)]); % stores the vorticity field
u_t     = zeros([size(D2),length(t_stamps)]); % stores the x_velocity field
v_t     = zeros([size(D2),length(t_stamps)]); % stores the y_velocity field

omega_t(:,:,sv_num) = omega; % store the initial field


% increment t and compute the vorticity field every dt
while t < tf + dt
    [omega,u,v] = integrate(omega,dt);
    t = t + dt;
    % save the vorticity at the set interval
    if t >= t_sv
        sv_num = sv_num + 1;
        omega_t(:,:,sv_num) = omega;
        u_t(:,:,sv_num) = u;
        v_t(:,:,sv_num) = v;
        t_sv = t_sv + dt_sv;
    end
end

file_name = sprintf('%d%s%d_%4.3f.mat',Nm,setup, ind,amp);
save(file_name, '-v7.3');
end

%% The Integrator
function [omega,u,v] = integrate(omega,dt)
global nu alpha F Dx Dy D2 ID2 
%% Linear + Forcing Advance

% advance linear and forcing terms by dt/2 (Crank-Nicolson)

Omega = fftshift(fft2(omega));

L  = nu*D2 - alpha;
E2 = 1./(1 - dt/4*L);
Omega = E2.*(dt/2*F + (1 + dt/4*L).*Omega);

Psi = -ID2.*Omega;

U = Dy.*Psi;
V = -Dx.*Psi;
u = real(ifft2(ifftshift(U)));
v = real(ifft2(ifftshift(V)));

%% Nonlinear Advance
% advance nonlinear terms by dt (Runge-Kutta 4)

omega  = real(ifft2(ifftshift(Omega)));
omegax = real(ifft2(ifftshift(Dx.*Omega)));
omegay = real(ifft2(ifftshift(Dy.*Omega)));

k1      = -(u.*omegax+v.*omegay);
om_tmp  = omega + dt/2*k1;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k2      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);
om_tmp  = omega + dt/2*k2;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k3      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);
om_tmp  = omega + dt*k3;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k4      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);

omega = omega + dt/6.*(k1 + 2.*(k2 + k3) + k4);

%% Linear + Forcing Advance
% advance linear and forcing terms by dt/2 (Crank-Nicolson)

Omega = fftshift(fft2(omega));

E2 = 1./(1 - dt/4*L);
Omega = E2.*(dt*F + (1 + dt/4*L).*Omega);

omega = real(ifft2(ifftshift(Omega)));
Psi = -ID2.*Omega;
U = Dy.*Psi;
V = -Dx.*Psi;
u = real(ifft2(ifftshift(U)));
v = real(ifft2(ifftshift(V)));

end

