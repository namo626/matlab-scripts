function plot_omega_DPS(filename, isVel)
% plots (animates) vorticity field of a DPS data, 
% optinally plots velocity vectors.



% (0 = no, 1 = yes)

% load(file_name,'alpha','omega_t','u_t','v_t','X','Y','t_stamps','amp','Nm','L0','ind','setup');
load(filename, 'omega_t', 'Nmx', 'Nmy', 'amp', 'alpha', 'X', 'Y', 'setup','tf','u_t','v_t')

dim = sprintf('%dx%d', Nmy, Nmx);

if isVel == 1
    flag = '_vel';
else
    flag = '';
end

v = VideoWriter(sprintf('%s%s_%.1fA_%da_%ds%s.avi', dim, setup, amp, alpha/0.01,tf,flag));
v.FrameRate = 15;
open(v);


for i = 1:size(omega_t,3) %#ok<NODEF>
    clf
    colormap jet
    colorbar
    caxis([-15 15])
%     imagesc(omega_t(:,:,i))
    h = pcolor(X,Y,omega_t(:,:,i));
    set(h,'EdgeColor','none')
    
    hold on
    if isVel == 1
        h = quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),u_t(1:10:end,1:10:end,i),v_t(1:10:end,1:10:end,i));
        set(h,'Color','k')
    end
    
    title(sprintf('(DPS), Nm = %s, I = %.2f A, alpha = %.2f, t = %1.2f', dim,amp,alpha,i)); 
    frame = getframe(gcf);
    writeVideo(v, frame);
    pause(0.001)
end
close(v);

end
