function avgWinDPS(filename, winSize, gran, isVel)
% plots a movie of the simulation data using window-averaging

% winSize - window size to average the frames
% if gran == 'discrete', skips frames by winSize
% isVel - plot velocity vectors? 1 = yes, 0 = no

% *** THIS IS THE MAIN DPS PLOTTER

load(filename, 'omega_t', 'Nmx', 'Nmy', 'amp', 'alpha', 'X', 'Y', 'setup','tf')
if isVel == 1
    load(filename, 'u_t','v_t')
end

[~, re, ~] = reynolds_number(filename);

% temporary tf
% tf = 1500;

% dimension of the box in terms of magnet amount

dim = sprintf('%dx%d', Nmy, Nmx);

if isequal(gran, 'discrete')
    frameRate = 2;
    stepSize = winSize;
    flag = 'D';
else
    frameRate = 15;
    stepSize = 1;
    flag = '';
end

if isVel == 1
    vflag = '_vel';
else
    vflag = '';
end

% name of the video file
v = VideoWriter(sprintf('%s%s%s_%.1fA_%da_%ds_%davg%s.avi', dim, setup, vflag,amp, alpha/0.01,tf,winSize,flag));

v.FrameRate = frameRate;
open(v);

if winSize == 1
    name = 'Vorticity Field'
else
    name = sprintf('Vorticity Field (%d Sec Average)', winSize);
end

% loop through flow field at each timestep, but skip by the window size
for i = winSize:stepSize:size(omega_t,3) %#ok<NODEF>
    clf
    colormap jet
    lowerBound = i - winSize + 1;
    h = pcolor(X,Y,mean(omega_t(:,:,lowerBound:i), 3));
    pbaspect([4 28 1]);
    set(h,'EdgeColor','none')
    caxis manual
    caxis([-15 15])

    hold on
    if isVel == 1
        h = quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),...
            mean(u_t(1:10:end,1:10:end,lowerBound:i),3),...
            mean(v_t(1:10:end,1:10:end,lowerBound:i),3));
        set(h,'Color','k')
        h.MaxHeadSize = 0.5;
        %h.AutoScaleFactor = 1.5;
    end

    %tt = title(sprintf('Nm = %s, F = %.2f, alpha = %.2f, t = %d-%d s', dim, amp, alpha, lowerBound, i));
    tt = title(sprintf(['%s\n\nHorizontal Current\nt = %d-%d s, Re = ' ...
                        '%d'], name, lowerBound, i, round(re)), 'fontsize', 18);

    % remove colorbar label
    c = colorbar;
    set(c, 'YTickLabel', []);

    % rotate title
    set(tt, 'Position', [0.14, 0.33], 'Rotation', 270);

    % remove ticks
    set(gca, 'xtick', []);
    set(gca, 'ytick', []);

    frame = getframe(gcf);
    frame.cdata = rot90(frame.cdata);

    writeVideo(v, frame);
    pause(0.001)
end
close(v);

end
