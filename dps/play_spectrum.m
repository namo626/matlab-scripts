function play_spectrum(k, e, name)

v = VideoWriter(name);
open(v);

for i = 1:size(e)
plot(k, e(i,:));
hold on
plot([178 178], [10^-20 10^10]);
hold off
text(200, 10^-5, 'k_{forcing}','FontSize',13);
title(sprintf('Energy Spectrum, t = %d',i));
xlabel('k','FontSize',13)
ylabel('E_{k}','FontSize',13)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
ylim([10^-20, 10^10])
frame = getframe(gcf);
writeVideo(v, frame);
pause(0.05)
end

close(v);

end