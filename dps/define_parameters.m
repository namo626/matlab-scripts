%---------------------define_parameters.m---------------------------------
% Written: Radford Mitchell
% Editted: Logan Kageorge
% Editted: Namo
% ***THIS IS THE MAIN DPS GENERATOR

% This function generates all the operator matrices that will be necessary
% to compute the flow evolution for DPS.

%-------------------------------------------------------------------------

function define_parameters(L0, Nmy, Nmx, Ngp, friction, setup)

% L0 - the length of each magnet
% Nmy - number of magnets in the y direction,
% Nmx - number of magnets in the x direction,
% Ngp - the number of grid points per magnet width
% friction - the rayleigh friction coefficient.
% Setup 1 is Kolmogorov, setup 2 is checkerboard.
%
% Example: define_parameters(0.025,30,6,32,0.05,2)

%------------------------------------------------------------------------
% INITIALIZATION

nu = 3.15E-6;  % effective kinematic viscosity
%nu = 7.35E-7; % same as NPS 1:1 nu ratio
kappa = pi/L0; % transverse wavenumber
alpha = friction; % Rayleigh friction
gl = L0 / Ngp;  % physical length of each grid space

%-------------------------------------------------------------------------
% GRID SETTINGS

if setup ~= 1 && setup ~= 2 && setup ~= 3
    disp('Setup option not properly chosen: Using Kolmogorov grid');
    setup = 1;
end

switch setup
  case 1
    Nx = 12*Ngp; % number of grid points in x direction
    Ny = Nmy*Ngp; % number of grid points in y direction
    Lx = 12*L0;  % width of the domain we are integrating over
    Ly = Nmy*L0;  % height of the domain we are inegrating over
  otherwise
    Nx = Nmx*Ngp; % number of grid points in x direction
    Ny = Nmy*Ngp; % number of grid points in y direction
    Lx = Nmx*L0;  % width of the domain we are integrating over
    Ly = Nmy*L0;  % height of the domain we are inegrating over

end

%-------------------------------------------------------------------------
x = (1:Nx)./Nx.*Lx; % define x vector corresponding to physical coordinates
y = (1:Ny)./Ny.*Ly; % define y vector corresponding to physical coordinates
[X, Y] = meshgrid(x,y);

% forcing profiles in functional form (no amplitudes)
switch setup
  case 1
    f0 = sin(kappa.*Y);
    setup = 'K';
  case 2
    f0 = sin(kappa.*Y).*sin(kappa.*X);
    setup = 'C';

%   case 3
%     signs = 2*randi([0,1],Nm,Nm)-1;
%     % Make one magnet
%     z = sin(kappa*X(1:Ngp,1:Ngp)).*sin(kappa*Y(1:Ngp,1:Ngp));
%     % Make the forcing profile
%     f0 = zeros(size(X));
%     for i = 1:Nm
%         for j = 1:Nm
%             f0(Ngp*(i-1)+1:Ngp*i,Ngp*(j-1)+1:Ngp*j) = signs(i,j)*z;
%         end
%     end
%     setup = 'R';
end

% Fourier transform of force
F0 = fftshift(fft2(f0)); %#ok<*NASGU> to prevent warnings
% x and y wavenumbers
qx = (-Nx/2:Nx/2-1)*2*pi/Lx;
qy = (-Ny/2:Ny/2-1)*2*pi/Ly;
% Fourier space derivative operators
Dx = 1i*repmat(qx,Ny,1);
Dy = 1i*repmat(qy',1,Nx);

D2  = Dx.^2 + Dy.^2;	% Laplacian

% invert the Laplacian (taking care of the central zero)
x_mid = Nx/2+1; % domain centers
y_mid = Ny/2+1;
ID2   = 1./D2;  % invert Laplacian
ID2(y_mid,x_mid) = 0; % set the inverse's center to 0 rather than Inf

Dx(:,1) = zeros(Ny,1);
Dy(1,:) = zeros(1,Nx);

disp(sprintf('Alpha = %.2f', alpha));
disp(sprintf('Nu = %.2e', nu));
disp(sprintf('Mode = %s', setup));

% saving everything
save('parameters.mat');
%edgar knobloch
%nathan katz

end
