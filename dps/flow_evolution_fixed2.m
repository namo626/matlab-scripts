%--------------------------flow_evolution_fixed2.m-------------------------------
% Written: Radford Mitchell
% Editted: Logan Kageorge
% Editted: Namo
%-------------------------------------------------------------------------
% This function evolves the flow from the input file name according to the parameters set in 
% define_parameters, using fixed timestepping. It uses the same integration scheme as
% flow_evolution_fixed.m
%
% ***Currently the first frame of a continued integration is a zero array. It should be
% manually deleted later.

%-------------------------------------------------------------------------

%% The Time-Stepper
function flow_evolution_fixed2(amp, tf, dT, filename1, savename)

% amp - forcing amplitude
% tf - final integration time
% dT - time step
% filename1 - filename of the previous simulation data to be further integrated
% savename - filename of the result


% Example: flow_evolution(0.15, 300, 1/32, 'flow1.mat', 'flow2.mat')

%-------------------------------------------------------------------------

% initialize the parameters as globals to access them in the integrator
global nu alpha F D2 ID2 Dx Dy %#ok<*NUSED>

% loads simulation parameters
load('parameters.mat');
dim = sprintf('%dx%d', Nmy, Nmx);
disp(sprintf('Continuing integration for +%d s', tf));
disp(['Box size is ' dim]);
disp(sprintf('Forcing = %.2f', amp));

% loads only the previous vorticity field to continue integration, since other
% values, e.g. u and v, follow from the vorticity field
load(filename1,'omega');

% Initialize forcing, time and the vorticity
F  = amp * F0;
t0 = 0;
t  = 0;
 
Omega = fftshift(fft2(omega));     % get the initial field in Fourier space
Psi   = -ID2.*Omega;               % get the stream function
U     = Dy.*Psi;                   % get the x_velocity in Fourier space
V     = -Dx.*Psi;                  % get the y_velocity in Fourier space
u     = real(ifft2(ifftshift(U))); % get the x_velocity in physical space
v     = real(ifft2(ifftshift(V))); % get the x_velocity in physical space

sv_num   = 1;          % counts the number of saves
dt_sv    = 1;          % how often we save the vorticity
t_stamps = 0:dt_sv:tf; % time stamps for the saved fields
t_sv     = t0 + dt_sv; % step the save timestamp

omega_t = zeros([size(D2),length(t_stamps)]); % stores the vorticity field
u_t     = zeros([size(D2),length(t_stamps)]); % stores the x_velocity field
v_t     = zeros([size(D2),length(t_stamps)]); % stores the y_velocity field

% omega_t(:,:,sv_num) = omega; % store the initial field


% increment t and compute the vorticity field every dt

% start measuring time
tic;
while t < tf + dT
    [omega,u,v] = integrate(omega,dT);
    t = t + dT;
    
    % save the vorticity at the set interval
    if t >= t_sv
        sv_num = sv_num + 1;
        omega_t(:,:,sv_num) = omega;
        u_t(:,:,sv_num) = u;
        v_t(:,:,sv_num) = v;
        t_sv = t_sv + dt_sv;
    end
   
end

disp(sprintf('Integration took %f', toc));



% file_name = sprintf('%d%s%d_%4.3f_%da_%ds.mat',Nm,setup, ind,amp,alpha/0.01,tf);
% save according to the manual input name instead
save(savename, '-v7.3');  % need mat v7.3 for >2GB
end

%% The Integrator
function [omega,u,v] = integrate(omega,dt)
global nu alpha F Dx Dy D2 ID2 
%% Linear + Forcing Advance

% advance linear and forcing terms by dt/2 (Crank-Nicolson)

Omega = fftshift(fft2(omega));

L  = nu*D2 - alpha;
E2 = 1./(1 - dt/4*L);
Omega = E2.*(dt/2*F + (1 + dt/4*L).*Omega);

Psi = -ID2.*Omega;

U = Dy.*Psi;
V = -Dx.*Psi;
u = real(ifft2(ifftshift(U)));
v = real(ifft2(ifftshift(V)));

%% Nonlinear Advance
% advance nonlinear terms by dt (Runge-Kutta 4)

omega  = real(ifft2(ifftshift(Omega)));
omegax = real(ifft2(ifftshift(Dx.*Omega)));
omegay = real(ifft2(ifftshift(Dy.*Omega)));

k1      = -(u.*omegax+v.*omegay);
om_tmp  = omega + dt/2*k1;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k2      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);
om_tmp  = omega + dt/2*k2;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k3      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);
om_tmp  = omega + dt*k3;
Om_tmp  = fftshift(fft2(om_tmp));
omx_tmp = real(ifft2(ifftshift(Dx.*Om_tmp)));
omy_tmp = real(ifft2(ifftshift(Dy.*Om_tmp)));
Psi_tmp = -ID2.*Om_tmp;
u_tmp   = real(ifft2(ifftshift(Dy.*Psi_tmp)));
v_tmp   = real(ifft2(ifftshift(-Dx.*Psi_tmp)));

k4      = -(u_tmp.*omx_tmp+v_tmp.*omy_tmp);

omega = omega + dt/6.*(k1 + 2.*(k2 + k3) + k4);

%% Linear + Forcing Advance
% advance linear and forcing terms by dt/2 (Crank-Nicolson)

Omega = fftshift(fft2(omega));

E2 = 1./(1 - dt/4*L);
Omega = E2.*(dt*F + (1 + dt/4*L).*Omega);

omega = real(ifft2(ifftshift(Omega)));
Psi = -ID2.*Omega;
U = Dy.*Psi;
V = -Dx.*Psi;
u = real(ifft2(ifftshift(U)));
v = real(ifft2(ifftshift(V)));

end

