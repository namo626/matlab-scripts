function fourier2 = fourierAnalyze(snapshots, numSnap)
  vals = linearizeSnapshot(snapshots, numSnap);
  [r, c] = size(vals);
  fourier = fft(vals,[],2);
  fourier2 = vals(:,1:end/2+1);
  
  for i = 1:r
    P = abs(fourier(i,:)/c);
    P = P(1:c/2+1);
    P(2:end-1) = 2*P(2:end-1);
    fourier2(i,:) = P;
  end
  
end
