function unc_ts(filenam,nu,dT)

% Load data
load([filenam]);

% Get mask for center region
magwidth = 1.27;
indx=find(abs(X_cm(1,:))/magwidth<4);
indy=find(abs(Y_cm(:,1))/magwidth<4);

% Crop data
U=U_cmps(indy,indx,:);
V=V_cmps(indy,indx,:);
X=X_cm(indy,indx)/magwidth;
Y=Y_cm(indy,indx)/magwidth;
%uv_unc=udrpc(indy,indx,:);

% Calculate rms velocities
for i=1:size(U,3)
    vyrms(i)=sqrt(sum(sum(V(:,:,i).^2))/(size(U,1)*size(U,2)));
    vrms(i)=sqrt(sum(sum(U(:,:,i).^2+V(:,:,i).^2))/(size(U,1)*size(U,2)));
end
%vyrms_unc=std(vyrms);
%vrms_unc=std(vrms);
%vrms=mean(vrms);
%vyrms=mean(vyrms);

% k=1;
% for i=1:size(U,1)
%     for j=1:size(U,2)
%         umean(k)=mean(squeeze(U(i,j,:)));
%         ustd(k)=std(squeeze(U(i,j,:)));
%         vmean(k)=mean(squeeze(V(i,j,:)));
%         vstd(k)=std(squeeze(V(i,j,:)));
%         k=k+1;
%     end
% end
% 
% vrms=sqrt((sum(sum(U(:,:,i).^2+V(:,:,i).^2)))/(size(U,1)*size(U,2)));
% a(i)=mean(uv_unc(:,:,i).^2);
% b(i)=std(uv_unc(:))^2;

% Calculate the uncertainty in rms velocity from PIV and dimensionalization
%vrms_unc=vrms*sqrt(((a+b)/vrms)^2+(2/768)^2);
vrms_unc=mean(vrms)*sqrt((std(vrms)/mean(vrms))^2+(2/768)^2);
vyrms_unc=mean(vyrms)*sqrt((std(vyrms)/mean(vyrms))^2+(2/768)^2);
vms_unc=sqrt(2)*vrms_unc;
vyms_unc=sqrt(2)*vyrms_unc;
vms=mean(vrms)^2;
vyms=mean(vyrms)^2;
vrms=mean(vrms);
vyrms=mean(vyrms);
bif1param=vyms/vms;
bif1param_unc=bif1param*sqrt((vms_unc/vms)^2+(vyms_unc/vyms)^2);

% Calculate the uncertainty in the viscosity from the uncertainty in
% temperature, dT, and uncertainty in viscosity measurement (1 second)
nu_unc=sqrt(((dT)*0.033)^2+(.00915)^2);

% Calculate the Reynolds number and the uncertainty in Re
re=vrms*magwidth/nu*100;
re_unc=re*sqrt((vrms_unc/vrms)^2+(nu_unc/nu)^2);

save([filenam(1:end-4) '_unc.mat']);

end
