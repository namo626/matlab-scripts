function [u_avg, v_avg] = avgVel(filename, ifwin)
% Simulation type: DPS
% Produces the spatial avg of velocity field in the y direction as a movie
% (y-dependent graph for each frame)

% filename - filename of the simulation data
% ifplot - display results

load(filename, 'u_t', 'v_t', 'dim', 'alpha', 'amp', 'setup')
y = size(u_t, 1);

t = size(u_t, 3);
u_avg = zeros(t, y);
v_avg = zeros(t, y);

for i = 1:t
  u_avg(i, :) = mean(u_t(:,:,i), 2);
  v_avg(i, :) = mean(v_t(:,:,i), 2);
end


% Reynold;s number
[~, Re, ~] = reynolds_number(filename);
if ifwin == 0
    v = VideoWriter(sprintf('%s%s_%.3f_%da_spatial.avi', dim, setup, amp, alpha/0.01));
    v.FrameRate = 15;
    open(v);
    for j = 1:t
        clf

        plot(1:y, u_avg(j,:), 'LineWidth', 2);
        hold on
        plot(1:y, v_avg(j,:));
        legend('x vel', 'y vel');
        xlabel('Y')

        title(sprintf('(DPS) Nm = %s, F = %.3f, alpha = %.2f, Re = %d, t = %ds', ...
                      dim, amp, alpha, round(Re), j));
        ylabel('Spatial avg of velocity')
        ylim([-0.15, 0.15])

        frame = getframe(gcf);
        writeVideo(v, frame);
        pause(0.001);
    end
    close(v);
else
    winSize = 10;
    v = VideoWriter(sprintf('%s%s_%.3f_%da_spatial_avg.avi', dim, setup, amp, alpha/0.01));
    v.FrameRate = 15;
    open(v);
    for j = winSize:t
        clf
        lowerBound = j - winSize + 1;
        plot(1:y, mean(u_avg(lowerBound:j,:)), 'LineWidth', 2);
        hold on
        plot(1:y, v_avg(j,:));
        legend('x vel', 'y vel');
        xlabel('Y')

        title(sprintf('(DPS) Nm = %s, F = %.3f, alpha = %.2f, Re = %d, t = %d-%ds', ...
                      dim, amp, alpha, round(Re), lowerBound, j));
        ylabel('Spatial avg of velocity')
        ylim([-0.15, 0.15])

        frame = getframe(gcf);
        writeVideo(v, frame);
        pause(0.001);
    end
    close(v);
end



end
