function plotAvgDPS(filename, transient_ends)
% plots the time-average vorticity field after transient

load(filename, 'omega_t', 'amp', 'alpha', 'Nmx', 'Nmy', 'tf', 'setup')

% temporary
tf = 1500;

dim = sprintf('%dx%d', round(Nmy), round(Nmx));


name = sprintf('Nm = %s, I = %.1f A, \alpha = %.2f', dim, amp, alpha/0.01);
savename = sprintf('%s%s_%.1fA_%da_%ds_avg.png', dim, setup, amp, alpha/0.01, tf);

imagesc(mean(omega_t(:,:,transient_ends:end), 3));
colormap jet
colorbar

title(name)

print(savename, '-dpng','-r300');

end