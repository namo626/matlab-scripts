function [ks, energies] = k_spectrum_omega_1D(filename, binSize, dimension)
% Calculates energy spectrum of DPS data for kx or ky (1D summation of fourier grid)
% ***This uses omega, not velocity field, to calculate the spectrum.
% Omega values are collapsed onto x or y axis by averaging.

% INPUT
% filename - filename of the DPS data
% binSize - wavenumber bin size (suggested binSize is 0.1)
% dimension - 'kx' or 'ky'


% OUTPUT
% ks - wavenumber row vector
% energies - energy matrix; each row represents each frame, 
% each column corresponds to each wavenumber


%% Initializing variables

load(filename, 'kappa', 'Nx', 'Ny', 'amp', 'gl', 'L0','omega_t')

% 1. For each flow snapshot, do FFT 
% 2. Generate a grid of kx and ky values, with the origin at the center
% 3. Generate, from (2), a 2D grid with values as magnitude of (kx,ky) at
% each point (should be a circle)
% 4. For each range of magnitude, sum all the values from the fourier grid
% whose positions lie in that range
% 5. The result is a row vector containing energy at each interval, or
% "bin"
% 6. Find that energy vector for each snapshot, and time-average to get the
% final
if strcmp(dimension, 'kx')
    Ngrid = Nx;
else
    Ngrid = Ny;
end

t = size(omega_t, 3);
k_grid = k_row;

% initializing spectrum of first time snapshot
fourier1 = avgFourier(omega_t(:,:,1));
% fourier1 = fourierU1.^2 + fourierV1.^2;

[energies1, k_int] = histogramize(k_grid, fourier1);

energies = zeros(t, length(energies1));
energies(1,:) = energies1;

for t_i = 2:t 
  fourier_i = avgFourier(omega_t(:,:,t_i));
  energies(t_i,:) = histogramize(k_grid, fourier_i);
end

ks = k_int;


%-----------------------------------------------------------------------------
%% Functions

    function fourier_vel = avgFourier(omega)
        temp_fourier = zeros(size(omega,1), size(omega,2));
        if strcmp(dimension, 'kx')

            
            for j = 1:size(omega,1)
                temp_fourier(j,:) = fftshift(abs(fft(omega(j,:))));

            end
            fourier_vel = mean(temp_fourier);
            
        else
            
            for j = 1:size(omega,2)
                temp_fourier(:,j) = fftshift(abs(fft(omega(:,j))));
            end
            fourier_vel = mean(temp_fourier, 2);
        end
    end
        

  function [energies, k_int] = histogramize(k_grid, fourier_grid)
    % The limit of k is the maximum value in the radius grid
    k_max = max(k_grid);
    
    binNum = ceil(k_max / binSize);
    % The energy row vector
    energies = zeros(1, binNum);
    k_int = zeros(1, binNum);  % will be used as x-axis in the plot
    for i = 1:binNum
      k1 = binSize*i;
      k2 = k1 + binSize;
%       k_int(i) = (k1 + k2)/2;
      k_int(i) = k2;
 
      energies(i) = sum(sum(fourier_grid(k1 <= k_grid & k_grid < k2)));

    end
  end

% generates a range of wavenumbers as a row vector
  function grid = k_row

    tx = 1:Ngrid/2;
    % Scale the grid into k wave numbers instead of merely grid positions
    % Factor is (1/grid length) * (1 / no. of gridpoints per axis)
    factor = (1/gl) * (1/Ngrid) * (2*pi);
    tx = tx * factor;
    grid = [ fliplr(tx) tx ];
  end

  function plot_k(ks, energies)
    plot(ks, energies);
    xlim([0, max(ks)]);
    title(sprintf('Energy spectrum #%d, Nm = %d, Forcing = %.1f, L0 = %.3f, binWidth = %.2f', ind, Nm, amp, L0, binSize));
    xlabel('$ \vert k \vert $', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('Energy');
  end

end
