function [ks, energies, fourier_t] = k_spectrum_vel(filename, binSize)
% k_spectrum :: matfile -> Double 
% -> 1D vector -> 2D matrix -> 2D matrix
% calculates energy spectrum of time snapshots of flow
% the spectrum is returned as a 2D matrix where each row is the spectrum
% of a snapshot


%% Initializing variables

load(filename, 'kappa', 'u_t', 'v_t', 'Nx', 'Nm', 'amp', 'ind', 'gl', 'L0')

  

% 1. For each flow snapshot, do FFT 
% 2. Generate a grid of kx and ky values, with the origin at the center
% 3. Generate, from (2), a 2D grid with values as magnitude of (kx,ky) at
% each point (should be a circle)
% 4. For each range of magnitude, sum all the values from the fourier grid
% whose positions lie in that range
% 5. The result is a row vector containing energy at each interval, or
% "bin"
% 6. Find that energy vector for each snapshot, and time-average to get the
% final
t = size(u_t, 3);
k_grid = radius_grid;

% initializing spectrum of first time snapshot
fourierU1 = fftshift(abs(fft2(u_t(:,:,1))));
fourierV1 = fftshift(abs(fft2(v_t(:,:,1))));

fourier1 = fourierU1.^2 + fourierV1.^2;

[energies1, k_int] = histogramize(k_grid, fourier1);

% 3D fourier domain array for each timestep
fourier_t = zeros(size(fourier1, 1), size(fourier1, 2), t);
fourier_t(:,:,1) = fourier1;

energies = zeros(t, length(energies1));
energies(1, :) = energies1;

for t_i = 2:t 
  fourierUi = fftshift(abs(fft2(u_t(:,:,t_i))));
  fourierVi = fftshift(abs(fft2(v_t(:,:,t_i))));
  fourier_i = fourierUi.^2 + fourierVi.^2;
  energies(t_i,:) = histogramize(k_grid, fourier_i);
  fourier_t(:,:,t_i) = fourier_i;
end

% energies = mean(energies);
ks = k_int;
fourier_t = mean(fourier_t, 3);

%plot_k(ks, energies);

%-----------------------------------------------------------------------------
%% Functions

  function [energies, k_int] = histogramize(k_grid, fourier_grid)
    % The limit of k is the maximum value in the radius grid
    k_max = max(k_grid(:));
    
    binNum = ceil(k_max / binSize);
    % The energy row vector
    energies = zeros(1, binNum);
    k_int = zeros(1, binNum);  % midpoint k values; will be used as x-axis
    for i = 0:binNum-1
      k1 = binSize*i;
      k2 = k1 + binSize;
      k_int(i+1) = (k1 + k2)/2;
      energies(i+1) = sum(sum(fourier_grid(k1 <= k_grid & k_grid < k2)));
      
    end
  end

  function grid = radius_grid
    % Generate only the first quadrant because of symmetry
    [tx, ty] = meshgrid(1:Nx/2, Nx/2:-1:1);
    % Scale the grid into k wave numbers instead of merely grid positions
    % Factor is (1/grid length) * (1 / no. of gridpoints per axis)
    factor = (1/gl) * (1/Nx) * (2*pi);
    tx = tx * factor;
    ty = ty * factor;
    grid = zeros(Nx/2, Nx/2);
    
    for i = 1:Nx/2
      for j = 1:Nx/2
        grid(i,j) = sqrt(tx(i,j)^2 + ty(i,j)^2);  % magnitude of k = distance from origin
      end
    end

    % making the total grid
    S = fliplr(grid); T = flipud(S); C = flipud(grid);
    grid = [ S grid; T C ];
  end

  function plot_k(ks, energies)
    plot(ks, energies);
    xlim([0, max(ks)]);
    title(sprintf('Energy spectrum #%d, Nm = %d, Forcing = %.1f, L0 = %.3f, binWidth = %.2f', ind, Nm, amp, L0, binSize));
    xlabel('$ \vert k \vert $', 'Interpreter', 'latex', 'FontSize', 14);
    ylabel('Energy');
  end

end
