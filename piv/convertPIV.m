%% DESCRIPTION
% This function converts a data from PIVlab by scaling the units of
% velocity field and length scale into MKS. Data is saved in the
% current directory with suffix '_mks'.
function convertPIV(filename)

load(filename, 'vorticity', 'u_component','v_component','x','y');

u_t = cell2mat(reshape(u_component,1,1,[]));
v_t = cell2mat(reshape(v_component,1,1,[]));
x1 = x{1};
y1 = y{1};

%% Define variables
inch = 0.0254;

% convert grid to meters
X = x1 * (inch/350);
Y = y1 * (inch/350);

% px/frame -> m/s (1 inch per ~350 px)
u_t = u_t * 60 * (inch/350);
v_t = v_t * 60 * (inch/350);

% dimension of the box (m)
Ly = 1*inch;
Lx = 7*inch;

% time between each snapshot (s)
dt = 1/60;

% vorticity
omega_t = zeros(size(u_t));
for i = 1:size(u_t,3)
    [curlz,~] = curl(X,Y,u_t(:,:,i),v_t(:,:,i));
    omega_t(:,:,i) = curlz;
end

save([filename(1:end-4) '_mks.mat'],'u_t','v_t','omega_t','X','Y','Ly','Lx','dt','-v7.3');