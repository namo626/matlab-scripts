function  [Out] = SubSlideMin(ImgIn,WindowSize,Plot)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Image processing - subtract sliding min (normilization) %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Out] = SubSlideMin(ImgIn,WindowSize,Plot)
%
% This function subrtacts the sliding min in a window of size WindowSize
%
%  ImgIn      -- Should be the image (not the file name)
%
%  WindowSize -- Size of the window the local min is calculated over
%                If no input is given a default window of 3x3 is used
%                The WindowSize should be roughly the particle diameter
%
%  Plot       -- (optional) 0 to not show results, 1 to show results
%                If no input is given no plot is shown
%
%
% Dependencies:
%               none
% 
% This code was writen by Chris Crowley on 10/29/0218

%% Taking care of a few things first
    if ~exist('WindowSize','var')
         % If no input was given for WindowSize use the default value
          WindowSize = 3;
    end
    if ~exist('Plot','var')
         % If no input was given for Plot, don't plot
          Plot = 0;
    end
    if mod(WindowSize,2)==0
        disp('..............................................................................')
        disp('I recommend useing an odd window size..')
        disp('    By choosing a window size that is even you are not symetricaly normalizing')
        disp('    and this will result in shifting the image over and down by 1 px (I think)')
        disp('..............................................................................')
    end
%% Actually doing the sliding average
% this slides the window to the right then down
    [m,n]=size(ImgIn);
    B=ImgIn;
    for ii=1:m+1-WindowSize
       B(ii,:)=min(ImgIn(ii:ii+WindowSize-1,:),[],1); 
    end
    for ii=1:n+1-WindowSize
       B(:,ii)=min(B(:,ii:ii+WindowSize-1),[],2); 
    end
%% Ploting
    if Plot == 1
        subplottight(1,2,1)
        imshow(ImgIn)
        title('Original image')
        subplottight(1,2,2)
        imshow(ImgIn-B)
        title('Output image')
    end

    Out = ImgIn-B;
end

%% Ploting function 
% this is to plot the images without huge ass margins
function h = subplottight(n,m,i);
    [c,r] = ind2sub([m n], i);
    ax = subplot('Position', [(c-1)/m, 1-(r)/n, 1/m, 1/n]);
    if(nargout > 0);
      h = ax;
    end
end
