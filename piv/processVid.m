%% DESCRIPTION
% This function loads the video file frame-by-frame, converting
% each frame to greyscale and enhancing it using SubSlideMin. The
% output is saved to the current directory as a video with the same
% frame rate. Use ffmpeg to extract it into images for PIV.

% Note: we need to install gst-libav for matlab to read MOV files
% on Arch Linux
function [imgs] = processVid(filename, ifsave, savename)
frames = 4000;
%frames = 340;

v = VideoReader(filename);
m = v.Height;
n = v.Width;

%% Cropping
%ymin = 400; ymax = 750;
ymin = floor(0.45*m); ymax = floor(0.9*m);
height = ymax-ymin+1;
imgs = zeros(height,n,frames,'uint8');

for i = 1:frames
    frame = readFrame(v);
    gray = rgb2gray(frame);
    gray = gray(ymin:ymax,:);

    % Enhance the image
    temp = 7*gray+50;
    imgs(:,:,i) = SubSlideMin(temp, 5, 0);


end

%imgs = 255*imgs;

%% Save to video
if ifsave
    w = VideoWriter(savename);
    w.FrameRate = 60;
    open(w);

    for i = 1:frames
        %imshow(imgs(:,:,i));
        writeVideo(w, imgs(:,:,i));
        pause(0.001);

    end

end