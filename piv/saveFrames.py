import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib
import h5py

'''This script converts a video file into a numpy array and saves
it to a file of the same name in the current directory.'''

cap = cv2.VideoCapture('/home/namo/Research/exps/DSC_1156.MOV')
height = int(cap.get(4))
width = int(cap.get(3))
#frames = int(cap.get(7))
frames = 100

# Save as hdf5
filename = 'test.h5'

with h5py.File(filename, 'w') as f:
    # create a dataset for your movie
    dst = f.create_dataset("myvideo",shape=(frames, height, width),
                           dtype=np.uint8)
    # fill the 10 frames with a random image
    for i in range(frames):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        dst[i] = gray
