%% DESCRIPTION
% This function calculates the power spectrum along a single
% direction of a given data containing flow snapshots in time.
function [ky,kx,Py,Px] = powerSpectrum(filename)
%% INPUT
% omega_t : 3d matrix containing snapshots of vorticity field
% Ly : height of the field
% Lx : width of the field

load(filename,'omega_t','Ly','Lx');

%% Average fft along each direction
[Ny,Nx,t] = size(omega_t);

% Subtract the mean flow
omega_y = omega_t - mean(omega_t, 1);
omega_x = omega_t - mean(omega_t, 2);

Fy = abs(fft(omega_y, Ny, 1)).^2;
Fx = abs(fft(omega_x, Nx, 2)).^2;

Fy_mean = mean(mean(Fy, 2), 3);
Fx_mean = mean(mean(Fx, 1), 3);

% Discard the second half
Py = Fy_mean(1:floor(Ny/2));
Px = Fx_mean(1:floor(Nx/2));

%% Define the (linear) wavenumber scale
ky = (0:(length(Py)-1)) / Ly;
kx = (0:(length(Px)-1)) / Lx;

end