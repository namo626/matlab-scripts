%% DESCRIPTION
% This function creates a video of the field obtained from PIVlab
% data. Output video is saved in the current directory.
function plotVel(filename, savename, sec, vor_method)
%% INPUT
% filename : path to PIV matfile
% savename : path to save the video
% sec  : number of seconds to average the flow field; use 0 if no averaging
% vor_method : 'curl' (matlab curl) or else use the one given by PIVlab


%% INITIALIZE
load(filename, 'vorticity', 'u_component','v_component','x','y','u_smoothed','v_smoothed');

u = cell2mat(reshape(u_component,1,1,[]));
v = cell2mat(reshape(v_component,1,1,[]));
frames = size(x,1);

% Calculating vorticity using given method
if strcmp(vor_method, 'curl')
    disp('Using matlab curl for vorticity');
    vor = zeros(size(u));
    for i = 1:frames
        [curlz,~] = curl(x{1},y{1},u(:,:,i),v(:,:,i));
        vor(:,:,i) = curlz;
    end
else
    disp('Using vorticity from PIVlab');
    vor = cell2mat(reshape(vorticity,1,1,[]));
end

% Choosing averaging window duration
if sec == 0
    winSize = 1;
else
    winSize = sec*60;
end

vid = VideoWriter(savename);
vid.FrameRate = 60;
open(vid);

cmin = min(min(min(vor)));
cmax = max(max(max(vor)));


%% PLOTTING

for i = winSize:frames
    clf
    colormap jet
    start = i - winSize + 1;
    h = pcolor(x{1},y{1}, mean(vor(:,:,start:i), 3));
    %h = pcolor(x{1},y{1}, zeros(size(x{1})));
    set(h, 'EdgeColor', 'none');
    pbaspect([7 1 1]);
    colorbar
    caxis manual
    caxis([-0.5, 0.5]);
    % axis equal

    hold on
    quiver(x{i},y{i},mean(u(:,:,start:i),3), mean(v(:,:,start:i),3), ...
           1.5);

    title(sprintf('t = %.3f-%.3f sec', start*(1/60),i*(1/60)));
    frame = getframe(gcf);
    writeVideo(vid, frame);
    pause(0.001);
end

disp('Done.');

end
