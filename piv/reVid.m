%% DESCRIPTION
% This function takes a PIV data containing the velocity fields and
% outputs the average Re over time. You must also specify the
% length scale and kinematic viscosity.
function [Re_avg, maxU] = reVid(filename, nu_mks, Ls_mks);

load(filename, 'vorticity', 'u_component','v_component');

u = cell2mat(reshape(u_component,1,1,[]));
v = cell2mat(reshape(v_component,1,1,[]));

%% Convert to m/s
u = u * 60 * (0.0254/350);
v = v * 60 * (0.0254/350);

% Storing characteristic velocity at each snapshot
[nx,ny,t] = size(u);
V = zeros(1,t);

for i = 1:t
    % crop out the edges because the velocity is zero there
    % u_vel = u_t(n0+1:end-n0, n0+1:end-n0, i);
    % v_vel = v_t(n0+1:end-n0, n0+1:end-n0, i);
    % V(i) = sqrt((sum(sum(u(:,:,i).^2 + v(:,:,i).^2))) / ((nx) * (ny)));
    V(i) = mean(mean(sqrt(u(:,:,i).^2+v(:,:,i).^2)));

end

V_avg = mean(V);
Re_avg = (Ls_mks * V_avg) / nu_mks;

maxU = max(max(max(u)));

end