# Compresses all .avi files to .mp4 in a specified destination directory

for i in *.avi;
do ffmpeg $2 -i "$i" -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" $1"${i%.*}.mp4"; 
done
