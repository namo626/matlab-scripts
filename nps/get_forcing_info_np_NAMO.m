function [return_val] = get_forcing_info_np_NAMO(I_mks,sense,query,Lx_mks)

%% Forcing amplitude, profile, finite difference, forcing info, non-periodic
inch = 2.54/100;

if(strcmp(query ,'amp'))
    % Here we define parameters that define forcing amplitude
    hc = 0.003;     % Height of electrolyte layer (mks)
    hd = 0.007;    % Height of dielectric layer
    dh = inch / 16; % air gap between magnet and electrolyte layer
    B1 = 65.8;     % slope of magnetic field
    B0 = 0.4 - B1*(dh + hd + hc);   % B-field at the bottom of dielectric layer (z = 0)
    Le = Lx_mks;    % width of electrodes (old value: 7 inch)
    B_mean = B0 + B1*hd + B1*hc/2  % mean B-field at the
                                   % electrolyte layer
    J = I_mks/Le/hc;  % Current Density
    F_mks = B_mean*J;  % Force Amplitude to be used in the
                       % depth-averaged model
    return_val = F_mks;
end

if(strcmp(query ,'amp_model2'))
    % Here we define parameters that define forcing amplitude
    hc = 0.003;                                     % Height of electrolyte layer (mks)
    hd = 0.003;                                     % Height of dielectric layer
    B0 = 0.2787;                                    % magnetic Field on Top of Magnets
    B1 = -30.54;                                    % Slope of the magnetic field
    Le = (6*inch+1*inch);                           % width of electrodes
    B_mean = (B0 + B1*hd + B1*hc/2)*hc/hc;     % B field integrated over the thickness
    J = I_mks/Le/hc;                                % Current Density
    F_mks = B_mean*J;                                   % Force Amplitude
    return_val = F_mks;
end
if(strcmp(query,'profile_exp_poly4'))
    display('Using experimental profile and four piece polynomial fit')
    load('grid_params.mat','xu','yu','Ly');
    load('sim_params.mat','Ls_mks','Nmp');
    % here we call a function that gives the x profile
    profile_x = generate_profile_x(xu,Ls_mks);
    % here we call a function that gives the y profile
    profile_y = generate_profile_y_poly4(yu,Ly,Nmp);

    mask_decay_x = repmat(profile_x,length(profile_y),1);
    mask_decay_y = repmat(profile_y',1,length(profile_x));
    F0x_wbp = sense*mask_decay_x.*mask_decay_y;
    F0x = F0x_wbp(2:end-1,2:end-1);
    return_val = F0x;

    save('forcing_profile_fdso.mat','profile_x','profile_y','xu','yu','F0x_wbp');
end

if(strcmp(query,'profile_exp_poly3'))
    display('Using experimental profile and three piece polynomial fit')
    load('grid_params.mat','xu','yu','Ly');
    load('sim_params.mat','Ls_mks','Nmp');
    % here we call a function that gives the x profile
    profile_x = generate_profile_x(xu,Ls_mks);
    % here we call a function that gives the y profile
    profile_y = generate_profile_y_poly3(yu,Ly,Nmp);

    mask_decay_x = repmat(profile_x,length(profile_y),1);
    mask_decay_y = repmat(profile_y',1,length(profile_x));
    F0x_wbp = sense*mask_decay_x.*mask_decay_y;
    F0x = F0x_wbp(2:end-1,2:end-1);
    return_val = F0x;

    save('forcing_profile_fdso.mat','profile_x','profile_y','xu','yu','F0x_wbp');
end

if(strcmp(query,'profile_point_dipole'))
    display('Using a point dipole profile')
    load('grid_params.mat','xu','yu');
    profile_file = sprintf('E:/Dropbox/2d_simulations_periodic/finite_diff/bfield_dipole_profile/kol_n0_20/Lx7in_Ly9in/depth_averaged/forcing_profile.mat');
    %profile_file = sprintf('E:/Dropbox/2d_simulations_periodic/finite_diff/bfield_dipole_profile/kol_n0_20/Lx7in_Ly6in/depth_averaged/forcing_profile.mat');

    %profile_file = 'E:/2d_simulation_secondary/test_cases/use_extreme_Bfield_profiles/z_6mm/forcing_profile.mat';
    %profile_file = 'E:/2d_simulation_secondary/test_cases/point_dipole_model/n032_nz16/5.00mm/forcing_profile.mat';
    %profile_file = 'E:/2d_simulation_secondary/test_cases/point_dipole_model/n032_nz16/8.00mm/forcing_profile.mat';
    %profile_file = sprintf('E:/Dropbox/2d_simulations_periodic/finite_diff/bfield_dipole_profile/kol_n0_20/Lx_6in_Ly_4in/forcing_profile.mat');

    try
        copyfile(profile_file);
        F0x_wbp = [];
    catch
        disp('The profile for this grid resolution does not seem to exist');
    end
    load('forcing_profile.mat','F0x_wbp');
    F0x_wbp = sense*F0x_wbp;
    F0x = F0x_wbp(2:end-1,2:end-1);
    return_val = F0x;

    save('forcing_profile.mat','F0x_wbp','xu','yu');
end

if(strcmp(query,'profile_point_dipole_cb'))
    display('Using a point dipole profile for chess board')
    load('grid_params.mat','xu','yu');
    profile_file = sprintf('E:/Dropbox/2d_simulations_periodic/finite_diff/bfield_dipole_profile/cb_n0_50/Nx4Ny5/depth_averaged/forcing_profile.mat');

    try
        copyfile(profile_file);
        F0x_wbp = [];
    catch
        disp('The profile for this grid resolution does not seem to exist');
    end
    load('forcing_profile.mat','F0x_wbp');
    F0x_wbp = sense*F0x_wbp;
    F0x = F0x_wbp(2:end-1,2:end-1);
    return_val = F0x;

    save('forcing_profile.mat','F0x_wbp','xu','yu');
end
end
%% In this function we compute the interpolated x profile of the Bfiend
function [profile_x] = generate_profile_x(xu,Ls_mks)

xexp_mks = [0,0.0787401574803150,0.157480314960630,0.236220472440945,0.314960629921260,...
    0.393700787401575,0.472440944881890,0.551181102362205,0.629921259842520,0.708661417322835,...
    0.787401574803150,0.866141732283465,0.944881889763780,1.02362204724409,1.10236220472441,...
    1.18110236220472,1.25984251968504,1.33858267716535,1.41732283464567,1.49606299212598,...
    1.57480314960630,1.65354330708661,1.73228346456693,1.81102362204724,1.88976377952756,...
    1.96850393700787,2.04724409448819]*1.27/100;
Bexp = [0,0.001023541184176,0.00297428831601123,0.00657865123342166,0.0128818508519441,...
    0.0237707106793588,0.0421575716066378,0.0724861257838432,0.120492882741165,0.193023576115970,...
    0.294873551062898,0.423633962254152,0.565204699533117,0.698038089373510,0.805048335652011,...
    0.881379771241050,0.930891857609281,0.961095024638219,0.978863406721844,0.988851871630324,...
    0.994173709004233,0.996944836916525,0.998271481887374,0.998888099823820,0.999531736639010,1,1];
% we displace the xexp by dx just to ensure that the first point in the
% interior of the domain has zero field
xexp = xexp_mks/Ls_mks;
% here we initialise the xprofile
profile_x = ones(1,length(xu));
% we collect all indices of points that make the decay profile
x_decay_ind = find(min(xexp)<=xu & xu<=max(xexp));
% we then read all x coordinates that form the decay
x_decay = xu(x_decay_ind);
% we interpolate experimental values into the grid coordinates
decay_prof_x = interp1(xexp,Bexp,x_decay,'spline');

profile_x(x_decay_ind) = decay_prof_x;
min_ind_decay = min(x_decay_ind);
max_ind_decay = max(x_decay_ind);
profile_x(min_ind_decay:-1:1) = 0;
profile_x(end:-1:end-max_ind_decay+1) = profile_x(1:max_ind_decay);
end

%% In this function we compute the model for Bfield decay in the y direction
function [profile_y] = generate_profile_y_poly3(yu,Ly,Nmp)
% All lengths are emasured in non-dimensional units

% width of magnet array in the y direction
Lmy = (2*Nmp);
% distance of magnet from side wall
wy_pad = (Ly-Lmy)/2;

% here we initialise the entire profile to a sine to start with
profile_y = sin(pi*(yu-wy_pad));

%% here we define the polynomial properties over 4 regions
% the idea is to divide the decay region into 4 subregions and fit a 7th
% degree polynomial in each of the regions using experimental data

% region 1
% the polynomial fit at the bottom end is f = -0.017y^2 - 0.0264y - 0.03
% the polynomial fit at top end is f = 0.2841y^2 - 0.7789y + 0.4374
r1yb = 0; r1yt = 1.375; r1ym = 1;
val_r1ym = -0.077;

% region 2 [1.375,2.5]
% The polynomial fit at the top is f = -2.126*y^2 + 10.59*y -12.29
r2yb = r1yt; r2yt = 2.5; r2ym = 1.747;
val_r2ym = 0;

% region 3 [2.5,3.5]
% fit at yt = 3.5 is , f =  2.934*y^2  -20.58*y + 35.02;
r3yb = r2yt; r3yt = 3.5; r3ym = 2.99;
val_r3ym = 0.03;

%% Here we generate the polynomials
% yb, yt, ym are the bottom, top and middle points
% y_decay_ind_rn are the indices in the nth region
% y_decay_rn are the y coordinates of the nth decay region

% here we fill in region 1
yb = r1yb; yt = r1yt; ym = r1ym;
y_decay_ind_r1 = find(yu<=yt);
y_decay_r1 = yu(y_decay_ind_r1);
bcy_r1 = [-0.017*yb^2 - 0.0264*yb - 0.03; -2*0.017*yb - 0.0264; -2*0.017; val_r1ym; ...
    0.2841*yt^2 - 0.7789*yt + 0.4374; 2*0.2841*yt - 0.7789; 2*0.2841];
profile_y(y_decay_ind_r1) = get_poly_fit(yb,yt,ym,bcy_r1,y_decay_r1);

% here we fill in region 2
yb = r2yb; yt = r2yt; ym = r2ym;
y_decay_ind_r2 = find(yb<=yu & yu<=yt);
y_decay_r2 = yu(y_decay_ind_r2);
bcy_r2 = [0.2841*yb^2 - 0.7789*yb + 0.4374; 2*0.2841*yb-0.7789; 2*0.2841 ; val_r2ym; ...
    -2.126*yt^2 + 10.59*yt - 12.29; -2*2.126*yt + 10.59; -2*2.126];
profile_y(y_decay_ind_r2) =  get_poly_fit(yb,yt,ym,bcy_r2,y_decay_r2);

% here we fill in region 3
yb = r3yb; yt = r3yt; ym = r3ym;
y_decay_ind_r3 = find(yb<=yu & yu<=yt);
y_decay_r3 = yu(y_decay_ind_r3);
bcy_r3 = [-2.126*yb^2 + 10.59*yb - 12.29; -2*2.126*yb + 10.59; -2*2.126; val_r3ym; ...
    sin(pi*yt); pi*cos(pi*yt); -pi^2*sin(pi*yt)];
profile_y(y_decay_ind_r3) = get_poly_fit(yb,yt,ym,bcy_r3,y_decay_r3);

max_ind_decay = max(y_decay_ind_r3);
profile_y(end:-1:end-max_ind_decay+1) = -profile_y(1:max_ind_decay);
end

%% In this function we compute the model for Bfield decay in the y direction
function [profile_y] = generate_profile_y_poly4(yu,Ly,Nmp)
% All lengths are emasured in non-dimensional units

% width of magnet array in the y direction
Lmy = (2*Nmp);
% distance of magnet from side wall
wy_pad = (Ly-Lmy)/2;

% here we initialise the entire profile to a sine to start with
profile_y = sin(pi*(yu-wy_pad));

%% here we define the polynomial properties over 4 regions
% the idea is to divide the decay region into 4 subregions and fit a 7th
% degree polynomial in each of the regions using experimental data

% region 1
% the polynomial fit at the bottom end is f = -0.017y^2 - 0.0264y - 0.03
% the polynomial fit at top end is f = 0.2841y^2 - 0.7789y + 0.4374
r1yb = 0; r1yt = 1.375; r1ym = 1;
val_r1ym = -0.077;

% region 2 [1.375,2.5]
% The polynomial fit at the top is f = -2.126*y^2 + 10.59*y -12.29
r2yb = r1yt; r2yt = 2.5; r2ym = 1.747;
val_r2ym = 0;

% region 3 [2.5,3.5]
% fit at yt = 3.5 is , f =  2.934*y^2  -20.58*y + 35.02;
r3yb = r2yt; r3yt = 3.5; r3ym = 2.99;
val_r3ym = 0.03;

% region 4 [3.5, 4]
r4yb = r3yt; r4yt = 4; r4ym = 3.647;
val_r4ym = -0.9916;

%% Here we generate the polynomials
% yb, yt, ym are the bottom, top and middle points
% y_decay_ind_rn are the indices in the nth region
% y_decay_rn are the y coordinates of the nth decay region

% here we fill in region 1
yb = r1yb; yt = r1yt; ym = r1ym;
y_decay_ind_r1 = find(yu<=yt);
y_decay_r1 = yu(y_decay_ind_r1);
bcy_r1 = [-0.017*yb^2 - 0.0264*yb - 0.03; -2*0.017*yb - 0.0264; -2*0.017; val_r1ym; ...
    0.2841*yt^2 - 0.7789*yt + 0.4374; 2*0.2841*yt - 0.7789; 2*0.2841];
profile_y(y_decay_ind_r1) = get_poly_fit(yb,yt,ym,bcy_r1,y_decay_r1);

% here we fill in region 2
yb = r2yb; yt = r2yt; ym = r2ym;
y_decay_ind_r2 = find(yb<=yu & yu<=yt);
y_decay_r2 = yu(y_decay_ind_r2);
bcy_r2 = [0.2841*yb^2 - 0.7789*yb + 0.4374; 2*0.2841*yb-0.7789; 2*0.2841 ; val_r2ym; ...
    -2.126*yt^2 + 10.59*yt - 12.29; -2*2.126*yt + 10.59; -2*2.126];
profile_y(y_decay_ind_r2) =  get_poly_fit(yb,yt,ym,bcy_r2,y_decay_r2);

% here we fill in region 3
yb = r3yb; yt = r3yt; ym = r3ym;
y_decay_ind_r3 = find(yb<=yu & yu<=yt);
y_decay_r3 = yu(y_decay_ind_r3);
bcy_r3 = [-2.126*yb^2 + 10.59*yb - 12.29; -2*2.126*yb + 10.59; -2*2.126; val_r3ym; ...
    2.934*yt^2 - 20.58*yt + 35.02;  2*2.934*yt  - 20.58;  2*2.934];
profile_y(y_decay_ind_r3) = get_poly_fit(yb,yt,ym,bcy_r3,y_decay_r3);


% here we fill in region 4
yb = r4yb; yt = r4yt; ym = r4ym;
y_decay_ind_r4 = find(yb<=yu & yu<=yt);
y_decay_r4 = yu(y_decay_ind_r4);
bcy_r4 = [2.934*yb^2 - 20.58*yb + 35.02;  2*2.934*yb - 20.58;  2*2.934; val_r4ym; ...
    sin(pi*yt); pi*cos(pi*yt); -pi^2*sin(pi*yt)];
profile_y(y_decay_ind_r4) = get_poly_fit(yb,yt,ym,bcy_r4,y_decay_r4);

max_ind_decay = max(y_decay_ind_r4);
profile_y(end:-1:end-max_ind_decay+1) = -profile_y(1:max_ind_decay);
end

function [poly_fit] = get_poly_fit(yb,yt,ym,bc,y)
poly_fit = zeros(size(y));

% here we initialise the matrix that contains the coefficients
% in this matrix we impose boundary conditions on the polynomial
% first 3 elements at r2yb indicate, f, f', f"
% next element f at r2ym
% next element f, f' and f'' at r2yt
DMATy = zeros(length(bc),length(bc));

DMATy(1,:) = [1         yb          yb^2        yb^3        yb^4        yb^5        yb^6];
DMATy(2,:) = [0         1           2*yb        3*yb^2      4*yb^3      5*yb^4      6*yb^5];
DMATy(3,:) = [0         0           2           6*yb        12*yb^2     20*yb^3     30*yb^4];
DMATy(4,:) = [1         ym          ym^2        ym^3        ym^4        ym^5        ym^6];
DMATy(5,:) = [1         yt          yt^2        yt^3        yt^4        yt^5        yt^6];
DMATy(6,:) = [0         1           2*yt        3*yt^2      4*yt^3      5*yt^4      6*yt^5];
DMATy(7,:) = [0         0           2           6*yt        12*yt^2     20*yt^3     30*yt^4];

coef = DMATy\bc;
for i = 1:length(coef)
    poly_fit = poly_fit + coef(i)*y.^(i-1);
end

end