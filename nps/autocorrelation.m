function C = autocorrelation(filename, name)

load(filename, 'U_t','V_t','nx','ny');

[U_t,V_t] = get_center_vel(U_t,V_t,nx,ny);

ydim = size(U_t, 1);
xdim = size(U_t, 2);
% tdim = size(U_t, 3);
tdim = 30;

% total velocity (column) vector
u_t = zeros(ydim*xdim*2, tdim);

for i = 1:tdim
    U_i = U_t(:,:,i);
    V_i = V_t(:,:,i);
    u_t(:,i) = [U_i(:); V_i(:)];
end

% mean flow column vector
u_avg = mean(u_t, 2);

u_delta = u_t - u_avg;

C = zeros(1, tdim-1);

% normalizer = mean(dot(u_delta,u_delta,1));
normalizer = zeros(1,tdim);

for T = 0:tdim-2
    temp_avg = zeros(1,tdim-T);
    for t = 1:(tdim-T)
        temp_avg(t) = dot(u_delta(t), u_delta(t+T));
        
        if T == 0
            normalizer(t) = dot(u_delta(t),u_delta(t));
        end
    end
    C(T+1) = mean(temp_avg);
end

C = C / mean(normalizer);
plotData;

    function plotData
        plot(0:length(C)-1, C,'Color','b')
        vec = 0:0.1:length(C)-1;
        line(vec, (1/exp(1))+zeros(1,length(vec)), 'LineStyle', '--')
        text(15, 0.42, '1/e','FontSize',17)
        title(name)
        xlabel('\tau', 'FontSize', 17)
        ylabel('C(\tau)', 'FontSize', 17)
    end

end