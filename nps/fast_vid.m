%--------------------------fast_vid.m-------------------------------------
% Written By Jeff Tithof
%-------------------------------------------------------------------------
% This function generates a flow field video for "fast processed" (OSIV)
% % data. The input "dt" is the amount of time between sequential frames, in
% seconds.
%
% Example: fast_vid(1)
%-------------------------------------------------------------------------

function fast_vid(dt,tagnum)

if nargin < 2
    tagnum = '';
end
% Load data
load(['corr_fast',tagnum,'.mat']);
avg_win=max([round(1/dt) 4]);
vs=1;
re=0;

% Get coordinates to use to crop
xind=find(U(round(size(U,1)/2),:,1)~=0);
yind=find(U(:,round(size(U,2)/2),1)~=0);

% Choose the region of the experimental data to graph (in terms of bins)
y_bin_min=yind(1); %Must be greater than or equal to 1
y_bin_max=yind(end); %Must be > y_bin_min and <= total number of bins
x_bin_min=xind(2); %Must be >= 1
x_bin_max=xind(end); %Must be > x_bin_min and <= total number of bins

% Input file name to save video as (do not remove .avi)
vid_file_name = ['fast_vid',tagnum,'.avi'];
try;delete(vid_file_name);catch;end;

% Rename variables according to convention, and select portion of data
% chosen above.
X2=X(y_bin_min:y_bin_max,x_bin_min:x_bin_max)/1.27;
Y2=Y(y_bin_min:y_bin_max,x_bin_min:x_bin_max)/1.27;
Z2=Omega(y_bin_min:y_bin_max,x_bin_min:x_bin_max,:);
U2=U(y_bin_min:y_bin_max,x_bin_min:x_bin_max,:);
V2=V(y_bin_min:y_bin_max,x_bin_min:x_bin_max,:);
clear Omega U V

% Get grid spacing in x and y directions
dx=X2(2,2)-X2(1,1);
dy=Y2(2,2)-Y2(1,1);

%Make video file to save frames in
writerObj = VideoWriter(vid_file_name);
hf=figure;
set(hf,'Color','white');
set(hf,'Color','white', 'Position', [100 100 750 750]);
open(writerObj);

% Set vorticity scale symmetrically
%z_max=max(max(max(max(Z2))),abs(min(min(min(Z2)))));
%z_max=z_max*0.5;
%if z_max>10
%    fprintf('maximum vorticity is %f: there may be an error',z_max)
z_max=3*std(Z2(:));
%end
z_inc = (2*z_max) / 25;
z_scale = -z_max:z_inc:z_max;

k=1;

% Loop over all time steps, drawing the flow and adding frames to video
for t=avg_win:avg_win:size(U2,3)
    
    % Clear figure from previous iteration and hold on
    clf;
    hold on;
    
    % Draw contour and set colorbar scale limits
    contourf(X2,Y2,mean(Z2(:,:,t-avg_win+1:t),3),z_scale,'LineStyle','none');
    set(gca,'clim',[-z_max  z_max]);
    colormap jet
    
    % Draw velocity field
    quiver(X2(1:vs:end,1:vs:end),Y2(1:vs:end,1:vs:end),mean(U2(1:vs:end,1:vs:end,t-avg_win+1:t),3),mean(V2(1:vs:end,1:vs:end,t-avg_win+1:t),3),'LineWidth',1,'Color',[0 0 0]);
    
    % Format labels and axes
    xlabel('','FontWeight','bold','FontSize',18,'FontName','Times');
    ylabel('','FontWeight','bold','FontSize',18,'FontName','Times');
    if re==0
        title(['t = ',num2str(fix((t)*dt)),...
            ' s'],'FontWeight','bold','FontSize',20,'FontName','Times');
    else
        title(['Re = ' num2str(re) ' , t = ',num2str(fix((t-1)*dt)),...
            ' s'],'FontWeight','bold','FontSize',20,'FontName','Times');
    end
    
    % Format the axes
    set(gca,'XTick',[],'YTick',[],'Layer','top','FontSize',...
        12,'FontName','Times','DataAspectRatio',[1 1 1]);
    
    % Turn the box off
    box off
    axis off
    
    % Set axis limits with a little spacing
    xlim([X2(1,1)-2*dx X2(end,end)+2*dx])
    ylim([Y2(1,1)-2*dy Y2(end,end)+2*dy])
    
    % Turn on the colorbar, format it, and shift it right a bit
    % Uncomment the following to turn the colorbar back on
    %cb = colorbar;
    %set(cb,'Position',get(cb,'Position')+[0.05 0 0 0]);
    %set(get(cb,'ylabel'),'String', '\omega','FontSize',...
    %    14,'FontWeight','bold','FontName','Times','rotation',0);
    %set(cb,'Ytick',[-floor(z_max):floor(z_max)/2:floor(z_max)],...
    %    'FontSize',14,'FontName','Times');
    
    %Add each frame to the video
    writeVideo(writerObj,getframe(gcf));
    k=k+1;
end

%Close video file and figure
close(writerObj);
close(hf);
% save('corr_fast','-append')
end