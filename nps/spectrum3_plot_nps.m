function spectrum3_plot_nps(k, energy, transient, k_start, k_left, k_right, plotTitle)
% spectrum_plot: plots mean energy spectrum of flow snapshots after
% transient. Shows curve fits for inverse casecade range and casecade
% range

% k_forcing = 178;
k_forcing = 4.45; % nondimensionalized
tolerance = 0.001;
ind_forcing = find(abs(k-k_forcing) < tolerance);
lim_start = find(abs(k-k_start) < tolerance);
lim_left = find(abs(k-k_left) < tolerance);
lim_right = find(abs(k-k_right) < tolerance);
% transient = 50; % this is overestimated, just for safety
mean_energy = mean(energy(transient:end, :));
% plot the overall spectrum
scatter(k, mean_energy, 5, 'filled');
title(plotTitle, 'FontSize', 13)
xlabel('wavenumber')
ylabel('Energy')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
hold on

% fitting the condensate range
k_cond = k(lim_start:lim_left);
e_cond = mean_energy(lim_start:lim_left);
pfit_cond = polyfit(log(k_cond), log(e_cond), 1);
% plot the curve fit
plot(k_cond, k_cond.^pfit_cond(1)*exp(1)^pfit_cond(2), 'LineWidth', 2);

if x == 2
    disp(2);
el

% fitting the inverse cascade range
k_inv = k(lim_left:ind_forcing);
e_inv = mean_energy(lim_left:ind_forcing);
pfit_inv = polyfit(log(k_inv), log(e_inv), 1);
% plot the curve fit
plot(k_inv, k_inv.^pfit_inv(1)*exp(1)^pfit_inv(2), 'LineWidth', 2);

% fitting the cascade range
k_cas = k(ind_forcing:lim_right);
e_cas = mean_energy(ind_forcing:lim_right);
pfit_cas = polyfit(log(k_cas), log(e_cas), 1);
% plot the curve fit
plot(k_cas, k_cas.^pfit_cas(1)*exp(1)^pfit_cas(2), 'LineWidth', 2, 'Color', 'cyan');


yl = ylim;
% plot forcing wavenumber vertical line
plot([k_forcing, k_forcing], yl)
xlim([10^(-1) 10^3])

legend_cond = sprintf('k^{%.2f}', pfit_cond(1));
legend_cas = sprintf('k^{%.2f}', pfit_cas(1));
legend_inv = sprintf('k^{%.2f}', pfit_inv(1));
legend({'data', legend_cond, legend_inv, legend_cas, 'forcing'}, 'FontSize',14)



end
