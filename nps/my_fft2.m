function [P,kx,ky] = my_fft2(f,x,y,num)
% Logan Kageorge, 2018
% This function generates an fft2 1-sided (P1) and 2-sided 0-mode shifted
% (P2) profile with the frequencies properly ordered. x and y should be
% column vectors of length Lx and Ly, and f should be a 2D matrix of size
% [Ly,Lx].

x = double(x); y = double(y);            % from single to double precision
Lx = length(x); Ly = length(y);          % number of pixels
dx = mean(diff(x)); dy = mean(diff(y));  % distance increment
ksx = 1/dx; ksy = 1/dy;                  % spatial sampling frequency
icx = ceil(Lx/2+1); icy = ceil(Ly/2+1);  % zero mode indices
kx = 2*pi*ksx*(0:(Lx/2))/Lx;             % spatial frequencies
ky = 2*pi*ksy*(0:(Ly/2))/Ly;

P2 = abs(fftshift(fft2(f)))/(Lx*Ly);     % 2-sided profile
P1 = P2(icy:end,icx:end,:);              % 1-sided profile
P1(2:end,2:end,:) = 4*P1(2:end,2:end,:); % Quadruple count quadrants
P1(1,2:end,:) = 2*P1(1,2:end,:);         % Double count axes
P1(2:end,1,:) = 2*P1(2:end,1,:);

kx2 = [-flip(kx(2:end)),kx(1:end-1)];    % spatial frequencies for P2
ky2 = [-flip(ky(2:end)),ky(1:end-1)];

if num == 1; P = P1;
elseif num == 2; P = P2; kx = kx2; ky = ky2;
else fprintf('num must be 1 or 2');
end

end