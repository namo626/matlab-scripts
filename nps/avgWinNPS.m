function avgWinNPS(filename, winSize)

%% Author: Namo
%% Parameters
% filename = filename of NPS
% winSize  = number of frames to average

% This function produces a video of the vorticity field averaged
% over the given winSize.

%% Simulation variables

load(filename, 'omega_t','U_t','V_t','Lx', 'Ly', 'I_mks',...
    'rayfric_mks', 'x', 'y','Nmx', 'Nmy', 'mode','tf_mks','nx','ny')

% Grid for velocity vectors
[X, Y] = meshgrid(x,y);
[a, b] = meshgrid(x(2:end), y(2:end));

% U and V fields at the center of each cell
[U_t, V_t] = get_center_vel(U_t, V_t, nx, ny);

% Average Reynold's number
[re, ~] = reynolds_number_nps(filename);

% Dimension of the box in terms of magnet
dim = sprintf('%dx%d', round(Ly), round(Lx));


%% Video settings
frameRate = 5;
stepSize = 1;

% Name of the video file
v = VideoWriter(sprintf('%s%s_%.3fA_%da_%ds_%davg.avi', dim, mode, I_mks, rayfric_mks/0.01, tf_mks,winSize));

v.FrameRate = frameRate;
open(v);

% Upper bound for the color
%limit = max(omega_t(:)) / 50;
limit = 120;


%% Main loop
% loop through flow field at each timestep, but skip by the window size
for i = winSize:stepSize:size(omega_t,3) %#ok<NODEF>
    clf
    colormap jet
    lowerBound = i - winSize + 1;
    h = pcolor(X,Y,mean(omega_t(:,:,lowerBound:i), 3));
    pbaspect([5 29 1]);
    set(h,'EdgeColor','none')
    caxis manual
    caxis([-limit, limit])

    hold on

    % Plot the velocity vector field
    h = quiver(a(1:10:end,1:10:end),b(1:10:end,1:10:end),...
        mean(U_t(1:10:end,1:10:end,lowerBound:i),3),...
        mean(V_t(1:10:end,1:10:end,lowerBound:i),3));
    set(h,'Color','k')
    h.MaxHeadSize = 0.5;
    h.AutoScaleFactor = 1.5;


    tt = title(sprintf('[NPS] Nm = %s, I = %.3f A, alpha = %.2f, t = %d-%d s', dim, I_mks, rayfric_mks, lowerBound, i));
    c = colorbar;
    set(c, 'YTickLabel', []);

    % rotate title
    set(tt, 'Position', [0.14, 0.33], 'Rotation', 270);

    % remove ticks
    set(gca, 'xtick', []);
    set(gca, 'ytick', []);

    frame = getframe(gcf);
    frame.cdata = rot90(frame.cdata);

    writeVideo(v, frame);
    pause(0.001)
end
close(v);

end