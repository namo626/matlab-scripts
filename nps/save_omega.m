function save_omega(file_name, t_start)
%clear;
load(file_name, 'alpha','omega_t','u_t','v_t','X','Y','t_stamps','amp','Nm','L0','ind','setup');

imagesc(mean(omega_t(:,:,t_start:end),3));
colormap jet;
colorbar;
% name = file_name(file_name >= 65 & file_name <= 90);


if setup == 'C'
  mode = 'Sinusoidal Checkerboard';
  
elseif setup == 'S'
  mode = 'Sinusoidal Checkerboard';
  
else
  mode = 'Randomized Checkerboard';
  
end

title(sprintf('Time-Avg %s #%d, amp = %.3f, Nm = %d, a = %.2f', mode, ind, amp, Nm, alpha));  
% print(sprintf('%s_%.2f', mode,amp), '-dpng','-r300');
text = [num2str(Nm), setup, num2str(ind), '_', num2str(amp), '_', num2str(alpha/0.01), 'a'];
print([text '.png'], '-dpng','-r300');


end
