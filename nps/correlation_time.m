function tc = correlation_time(filename)
corr = correlation_get(filename);
cdiff = abs(exp(1) - corr);
tc = find(cdiff == min