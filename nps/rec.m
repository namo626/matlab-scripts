%--------------------------rec.m------------------------------------------
% Written By Jeff Tithof
%-------------------------------------------------------------------------
% This function generates a normalized recurrence plot using
% the velocity field loaded from "flnm"
%-------------------------------------------------------------------------

% Clear all and load the correlation data
function rec(flnm, name)

% Load input data
load(flnm, 'U_t', 'V_t','nx','ny');
[U_cmps,V_cmps] = get_center_vel(U_t,V_t,nx,ny);

% Initialize the recurrence structure
bins = size(U_t, 3);

rec_mat = zeros(bins,bins,'single');

% Open a figure
% hf = figure; 

% Calculate the norm of the differences in vector fields

for i=1:bins
    vel_norm(i)=sum(sum(U_cmps(:,:,i).^2+V_cmps(:,:,i).^2));
end
vel_norm=max(vel_norm);
for i = 1:bins-1
    if mod(i,500)==0
        disp(['Processing step ' num2str(i)])
    end
    for j = 1:(bins-i)
        rec_mat(i,j)=sqrt(sum(sum((U_cmps(:,:,i)-U_cmps(:,:,i+j)).^2+(V_cmps(:,:,i)-V_cmps(:,:,i+j)).^2))/vel_norm);
    end
end

% Transpose recurrence matrix
rec_mat=rec_mat';

% Make vectors to label the correct time on the x and y axes
% x=0:dt:bins*dt;
% y=x;
x = 1:bins;
y = x;
% Plot figure and format labels and axes
imagesc(x,y,rec_mat);
colormap jet;
title(name);
colorbar;
set(gca,'clim',[0 max(max(rec_mat))]);
xlabel('t (s)','FontWeight','bold','FontSize',18,'FontName','Times');
ylabel('{\tau} (s)','FontWeight','bold','FontSize',18,'FontName','Times');
set(gca,'YDir','normal');
% set(gca,'Layer','top','FontSize',14,'FontName','Times',...
%     'DataAspectRatio',[1 1 1]);
% 
% % Save data and image
% save(['rec_' flnm(1:end-4) '.mat']);
% export_fig(['rec_' flnm(1:end-4) '.tif'],'-transparent');
% export_fig(['rec_' flnm(1:end-4) '.pdf'],'-transparent');

end