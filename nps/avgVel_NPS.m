function [u_avg, v_avg] = avgVel_NPS(filename, ifwin)
% Simulation type: NPS
% Produces the spatial avg of velocity field in the y direction
% (y-dependent graph) for each frame

% If y is shorter than x, then it is x-dependent

    load(filename, 'U_t', 'V_t','rayfric_mks', 'I_mks','nx','ny','Lx','Ly','mode')
    [u_t, v_t] = get_center_vel(U_t, V_t, nx, ny);
    dim = sprintf('%dx%d', round(Ly), round(Lx));
    y = size(u_t, 1);
    x = size(u_t, 2);
    t = size(u_t, 3);

    vertical = Ly >= Lx;


    if vertical
        u_avg = zeros(t, y);
        v_avg = zeros(t, y);
    else
        u_avg = zeros(t, x);
        v_avg = zeros(t, x);
    end


    % reynold's number
    [~, re, ~] = reynolds_number_nps(filename);


    for i = 1:t
        if vertical
            u_avg(i, :) = mean(u_t(:,:,i), 2);
            v_avg(i, :) = mean(v_t(:,:,i), 2);
        else
            u_avg(i, :) = mean(u_t(:,:,i), 1);
            v_avg(i, :) = mean(v_t(:,:,i), 1);
        end

    end


    if ifwin == 0
        v = VideoWriter(sprintf('%s%s_%.1fA_%da_spatial.avi', dim, mode, I_mks, rayfric_mks/0.01));
        v.FrameRate = 15;
        open(v);
        for j = 1:t
            clf

            plot(1:y, u_avg(j,:), 'LineWidth', 2);
            hold on
            plot(1:y, v_avg(j,:));
            legend('x vel', 'y vel');
            xlabel('Y')

            title(sprintf('(NPS) Nm = %s, I = %.3f A, alpha = %.2f, Re = %d t = %ds', ...
                          dim, I_mks, rayfric_mks, round(re), j));
            ylabel('Spatial avg of velocity')
            ylim([-0.15, 0.15])

            frame = getframe(gcf);
            writeVideo(v, frame);
            pause(0.001);
        end
        close(v);
    else
        winSize = 10;
        v = VideoWriter(sprintf('%s%s_%.3fA_%da_spatial_avg.avi', ...
                                dim, mode, I_mks, rayfric_mks/0.01));
        v.FrameRate = 15;
        open(v);
        for j = winSize:t
            clf
            lowerBound = j - winSize + 1;
            if vertical
                plot(1:y, mean(u_avg(lowerBound:j,:)), 'LineWidth', 2);
                hold on
                plot(1:y, v_avg(j,:));
                xlabel('Y')
            else
                plot(1:x, mean(u_avg(lowerBound:j, :)), 'LineWidth', ...
                     2);
                hold on
                plot(1:x, mean(v_avg(lowerBound:j, :)), 'LineWidth', ...
                     2);
                xlabel('X')
            end

            ylim([-4.5 4.5])

            legend('x vel', 'y vel');
            title(sprintf('(NPS) Nm = %s, I = %.3f A, alpha = %.2f, Re = %d, t = %d-%ds', ...
                          dim, I_mks, rayfric_mks, round(re), lowerBound, j));
            ylabel('Spatial avg of velocity')

            %if Lx > Ly
            %    ylim([-0.5, 0.5])
            %else
            %    ylim([-2.5, 2.5])
            %end

            frame = getframe(gcf);
            writeVideo(v, frame);
            pause(0.001);
        end
        close(v);
    end




end
