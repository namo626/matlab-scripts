function plotNPS(filename, mode)

load(filename, 'omega_t', 'I_mks', 'tf_mks', 'x', 'y', 'rayfric_mks', 'Re_nu', 'Nm','Ly','Lx')

[X, Y] = meshgrid(x,y);

if mode == 'R'
  dim = sprintf('%dx%d', round(Ly), round(Lx));
else
  dim = num2str(Nm);
end

v = VideoWriter(sprintf('%sM_%.1fA_%da_%ds.avi', dim, I_mks, rayfric_mks/0.01, tf_mks));
v.FrameRate = 15;
open(v);


for i = 1:size(omega_t,3) %#ok<NODEF>
    clf
    colormap jet
%     colorbar
%     imagesc(omega_t(:,:,i))
    h = pcolor(X,Y,omega_t(:,:,i));
    set(h,'EdgeColor','none')
    caxis manual
    caxis([-60 60])
    
    hold on
%     h = quiver(X(1:10:end,1:10:end),Y(1:10:end,1:10:end),u_t(1:10:end,1:10:end,i),v_t(1:10:end,1:10:end,i));
%     set(h,'Color','k')
    title(sprintf('Nm = %s, I = %d A, \alpha = %.2f, t = %1.2f s', dim, I_mks, rayfric_mks, i)); 
    colorbar
    frame = getframe(gcf);
    
    writeVideo(v, frame);
    pause(0.001)
end
close(v);

end
