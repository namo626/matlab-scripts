function plotAvgNPS(filename, transient_ends)
% plots the time-average vorticity field after transient

load(filename, 'omega_t', 'I_mks', 'rayfric_mks', 'Nm', 'Lx', 'Ly', 'tf_mks', 'mode')

if mode == 'R'
  dim = sprintf('%dx%d', round(Ly), round(Lx));
else
  dim = num2str(Nm);
end

name = sprintf('Nm = %s, I = %.1f A, \alpha = %.2f', dim, I_mks, rayfric_mks/0.01);
savename = sprintf('%sM_%.1fA_%da_%ds_avg.png', dim, I_mks, rayfric_mks/0.01, tf_mks);

imagesc(mean(omega_t(:,:,transient_ends:end), 3));
colormap jet
colorbar

title(name)

print(savename, '-dpng','-r300');

end