function spectrum_plot(k, energy, k_lleft, k_lright, k_rleft, k_rright, plotTitle)
% spectrum_plot: plots mean energy spectrum of flow snapshots after
% transient. Shows curve fits for inverse casecade range and casecade
% range

% *** This function is obsolete because we now separate kx and ky

% k_forcing = 178;
transient = 10;
k_forcing = 4.45; % nondimensionalized
tolerance = 0.1;
ind_forcing = find(abs(k-k_forcing) < tolerance);
lim_lleft = find(abs(k-k_lleft) < tolerance);
lim_lright = find(abs(k-k_lright) < tolerance);
lim_rleft = find(abs(k-k_rleft) < tolerance);
lim_rright = find(abs(k-k_rright) < tolerance);
mean_energy = mean(energy(transient:end, :));

% plot the overall spectrum
scatter(k, mean_energy, 50, 'filled');
title(plotTitle, 'FontSize', 13)
xlabel('k', 'FontSize', 15)
ylabel('Power', 'FontSize', 15)
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
set(gca, 'FontSize', 15)
hold on

% fitting the inverse cascade range
k_inv = k(lim_lleft:lim_lright);
e_inv = mean_energy(lim_lleft:lim_lright);
pfit_inv = polyfit(log(k_inv), log(e_inv), 1);
% plot the curve fit
plot(k_inv, k_inv.^pfit_inv(1)*exp(1)^pfit_inv(2), 'LineWidth', 3);

% fitting the cascade range
k_cas = k(lim_rleft:lim_rright);
e_cas = mean_energy(lim_rleft:lim_rright);
pfit_cas = polyfit(log(k_cas), log(e_cas), 1);
% plot the curve fit
plot(k_cas, k_cas.^pfit_cas(1)*exp(1)^pfit_cas(2), 'LineWidth', 3);


ylim([10^(-8), 10^2])
yl = ylim;
% plot forcing wavenumber
plot([k_forcing, k_forcing], yl, '--', 'LineWidth', 3)
xlim([10^(-1) 10^3])

k_max = pi*32;
plot([k_max, k_max], yl, '--', 'LineWidth', 2);

k_box = 2*pi/4;
plot([k_box k_box], yl, '--', 'LineWidth', 2);

legend_cas = sprintf('k^{%.2f}', pfit_cas(1));
legend_inv = sprintf('k^{%.2f}', pfit_inv(1));
legend({'data', legend_inv, legend_cas, 'forcing'}, 'FontSize',14)



end
