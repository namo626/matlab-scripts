function spectrum_plot_1D(transient,k, energyx, k_leftx, k_rightx,energyy,k_lefty,k_righty,plotTitle)
% spectrum_plot: plots mean energy spectrum of flow snapshots after
% transient. Shows curve fits for inverse casecade range and casecade
% range (NPS)

% This plots both kx and ky

% k_forcing = 178;
k_forcing = 3.15; % nondimensionalized
tolerance = 0.01;
ind_forcing = find(abs(k-k_forcing) < tolerance);

% for kx
lim_leftx = find(abs(k-k_leftx) < tolerance);
lim_rightx = find(abs(k-k_rightx) < tolerance);
mean_energyx = mean(energyx(transient:end, :));

% for ky
lim_lefty = find(abs(k-k_lefty) < tolerance);
lim_righty = find(abs(k-k_righty) < tolerance);
mean_energyy = mean(energyy(transient:end, :));

% plot the overall spectrum
scatter(k, mean_energyx, 20, 'filled');
hold on
scatter(k, mean_energyy, 20, 'filled');
title(plotTitle, 'FontSize', 13)
xlabel('wavenumber')
ylabel('Energy')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

% fitting the inverse cascade range
% for kx
k_invx = k(lim_leftx:ind_forcing);
e_invx = mean_energyx(lim_leftx:ind_forcing);
pfit_invx = polyfit(log(k_invx), log(e_invx), 1);
% plot the curve fit
plot(k_invx, k_invx.^pfit_invx(1)*exp(1)^pfit_invx(2), 'LineWidth', 2);

% for ky
k_invy = k(lim_lefty:ind_forcing);
e_invy = mean_energyy(lim_lefty:ind_forcing);
pfit_invy = polyfit(log(k_invy), log(e_invy), 1);
% plot the curve fit
plot(k_invy, k_invy.^pfit_invy(1)*exp(1)^pfit_invy(2), 'LineWidth', 2);


% fitting the cascade range
% for kx
k_casx = k(ind_forcing:lim_rightx);
e_casx = mean_energyx(ind_forcing:lim_rightx);
pfit_casx = polyfit(log(k_casx), log(e_casx), 1);
% plot the curve fit
plot(k_casx, k_casx.^pfit_casx(1)*exp(1)^pfit_casx(2), 'LineWidth', 2);

% for ky
k_casy = k(ind_forcing:lim_righty);
e_casy = mean_energyy(ind_forcing:lim_righty);
pfit_casy = polyfit(log(k_casy), log(e_casy), 1);
% plot the curve fit
plot(k_casy, k_casy.^pfit_casy(1)*exp(1)^pfit_casy(2), 'LineWidth', 2);


yl = ylim;
% plot forcing wavenumber
plot([k_forcing, k_forcing], yl)
xlim([10^(-1) 10^3])

legend_casx = sprintf('k^{%.2f}', pfit_casx(1));
legend_invx = sprintf('k^{%.2f}', pfit_invx(1));
legend_casy = sprintf('k^{%.2f}', pfit_casy(1));
legend_invy = sprintf('k^{%.2f}', pfit_invy(1));
legend({'kx','ky', legend_invx, legend_casx, legend_invy,legend_casy, 'forcing'}, 'FontSize',14)



end
