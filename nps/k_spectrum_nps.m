function [ks, energies] = k_spectrum_nps(filename, binSize)
% k_spectrum_nps :: matfile -> Double
% -> 1D vector -> 2D matrix -> 2D matrix
% calculates energy spectrum of time snapshots of flow
% the spectrum is returned as a 2D matrix where each row is the spectrum
% of a snapshot

% default binSize is 0.1
%*** This function is obsolete because we now separate kx and ky

%% Initializing variables

load(filename, 'omega_t', 'U_t', 'V_t', 'Ls_mks','n0','nx','ny', ...
     'x', 'y');

% temporary initializations
% n0 = 32; % bins per magnet
% gl = Ls_mks / n0; % grid length
gl = 1 / n0;
% Nx will be used to generate the initial square grid, assuming that
% Nx >= Ny
Nx = nx;
Ny = ny;

% Ngrid is the size of the square grid that will be made
if Nx > Ny
    Ngrid = Nx;
else
    Ngrid = Ny;
end

[U_t, V_t] = get_center_vel(U_t, V_t, nx, ny);

% 1. For each flow snapshot, do FFT
% 2. Generate a grid of kx and ky values, with the origin at the center
% 3. Generate, from (2), a 2D grid with values as magnitude of (kx,ky) at
% each point (should be a circle)
% 4. For each range of magnitude, sum all the values from the fourier grid
% whose positions lie in that range
% 5. The result is a row vector containing energy at each interval, or
% "bin"
% 6. Find that energy vector for each snapshot, and time-average to get the
% final

%vel_t = sqrt(U_t.^2 + V_t.^2);
%vel_t = U_t.^2 + V_t.^2;
t = size(U_t, 3);
k_grid = radius_grid;
x = x(2:end);
y = y(2:end);


% initializing spectrum of first time snapshot
% all velocity values are ones in the center of each cell

fourierU1 = my_fft2(U_t(:, :, 1), x, y, 2);
fourierV1 = my_fft2(V_t(:, :, 1), x, y, 2);
fourier1 = fourierU1.^2 + fourierV1.^2;
% [fourier1, ~, ~] = my_fft2(vel_t(:, :, 1), x, y, 2);
[energies1, k_int] = histogramize(k_grid, fourier1);

% 3D fourier domain array for each timestep
fourier_t = zeros(size(fourier1, 1), size(fourier1, 2), t);
fourier_t(:,:,1) = fourier1;

energies = zeros(t, length(energies1));
energies(1, :) = energies1;

for t_i = 2:t
    fourierU_i = my_fft2(U_t(:,:,t_i), x, y, 2);
    fourierV_i = my_fft2(V_t(:,:,t_i), x, y, 2);
    fourier_i = sqrt(fourierU_i.^2 + fourierV_i.^2);
    %[fourier_i, ~, ~] = my_fft2(vel_t(:,:,t_i), x, y, 2);
    energies(t_i,:) = histogramize(k_grid, fourier_i.^2);
    fourier_t(:,:,t_i) = fourier_i.^2;
end

% energies = mean(energies);
ks = k_int;
fourier_t = mean(fourier_t, 3);

%plot_k(ks, energies);

%-----------------------------------------------------------------------------
%% Functions

  function [energies, k_int] = histogramize(k_grid, fourier_grid)
    % The limit of k is the maximum value in the radius grid
    k_max = max(k_grid(:));
  %k_max = 2*pi/

    binNum = ceil(k_max / binSize);
    % The energy row vector
    energies = zeros(1, binNum);
    k_int = zeros(1, binNum);  % midpoint k values; will be used as x-axis
    for i = 0:binNum-1
      k1 = binSize*i;
      k2 = k1 + binSize;
      k_int(i+1) = (k1 + k2)/2;
      energies(i+1) = sum(sum(fourier_grid(k1 <= k_grid & k_grid < k2)));

    end
  end

  function grid = radius_grid
    % Generate only the first quadrant because of symmetry
    [tx, ty] = meshgrid(1:Ngrid/2, Ngrid/2:-1:1);
    % Scale the grid into k wave numbers instead of merely grid positions
    % Factor is (1/grid length) * (1 / no. of gridpoints per axis) * 2pi
    factor = (1/gl) * (1/Ngrid) * (2*pi);
    tx = tx * factor;
    ty = ty * factor;
    grid = zeros(Ngrid/2, Ngrid/2);

    for i = 1:Ngrid/2
      for j = 1:Ngrid/2
        grid(i,j) = sqrt(tx(i,j)^2 + ty(i,j)^2);  % magnitude of k = distance from origin
      end
    end

    % making the total grid
    S = fliplr(grid); T = flipud(S); C = flipud(grid);
    grid = [ S grid; T C ];

    % synchronizing grid size and actual box size
    % if they are the same, then this has no effect
    if Nx > Ny
        start = (nx - ny + 2) / 2;
        finish = start + ny - 1;
        grid = grid(start:finish, :);
    else
        start = (ny - nx + 2) / 2;
        finish = start + nx - 1;
        grid = grid(:, start:finish);
    end
  end

%   function plot_k(ks, energies)
%     plot(ks, energies);
%     xlim([0, max(ks)]);
%     title(sprintf('Energy spectrum #%d, Nm = %d, Forcing = %.1f, L0 = %.3f, binWidth = %.2f', ind, Nm, amp, L0, binSize));
%     xlabel('$ \vert k \vert $', 'Interpreter', 'latex', 'FontSize', 14);
%     ylabel('Energy');
%   end

end
