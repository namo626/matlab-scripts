%---------------------define_parameters.m---------------------------------
% Written: Radford Mitchell
% Editted: Logan Kageorge
%-------------------------------------------------------------------------
% This function generates all the operator matrices that will be necessary
% to compute the flow evolution. L0 in the periodic case is the length of 
% the magnets, Nm is the number of magnets in the y direction, and Ngp is 
% the number of grid points per magnet width.
% Setup 1 is Kolmogorov, setup 2 is checkerboard
%
% Example: define_parameters(0.025,4,32,1)
%-------------------------------------------------------------------------

function define_parameters(L0,Nm,Ngp,friction,setup)

nu = 3.15E-6;  % effective kinematic viscosity
kappa = pi/L0; % transverse wavenumber
% alpha = 0.07;  % Rayleigh friction coefficient
alpha = friction;
gl = L0 / Ngp;  % physical length of each grid space

%-------------------------------------------------------------------------
% GRID SETTINGS

if setup ~= 1 && setup ~= 2 && setup ~= 3
    disp('Setup option not properly chosen: Using Kolmogorov grid');
    setup = 1;
end

switch setup
  case 1
    Nx = 12*Ngp; % number of grid points in x direction
    Ny = Nm*Ngp; % number of grid points in y direction
    Lx = 12*L0;  % width of the domain we are integrating over
    Ly = Nm*L0;  % height of the domain we are inegrating over
  otherwise
    Nx = Nm*Ngp; % number of grid points in x direction
    Ny = Nm*Ngp; % number of grid points in y direction
    Lx = Nm*L0;  % width of the domain we are integrating over
    Ly = Nm*L0;  % height of the domain we are inegrating over

end

%-------------------------------------------------------------------------
x = (1:Nx)./Nx.*Lx; % define x vector corresponding to physical coordinates
y = (1:Ny)./Ny.*Ly; % define y vector corresponding to physical coordinates
[X, Y] = meshgrid(x,y);

% forcing profiles in functional form (no amplitudes)
switch setup
  case 1
    f0 = sin(kappa.*Y); 
    setup = 'K';
  case 2
    f0 = sin(kappa.*Y).*sin(kappa.*X);
    setup = 'C';
    
  case 3
    signs = 2*randi([0,1],Nm,Nm)-1;
    % Make one magnet
    z = sin(kappa*X(1:Ngp,1:Ngp)).*sin(kappa*Y(1:Ngp,1:Ngp));
    % Make the forcing profile
    f0 = zeros(size(X));
    for i = 1:Nm
        for j = 1:Nm
            f0(Ngp*(i-1)+1:Ngp*i,Ngp*(j-1)+1:Ngp*j) = signs(i,j)*z;
        end
    end
    setup = 'R';
end

% Fourier transform of force
F0 = fftshift(fft2(f0)); %#ok<*NASGU> to prevent warnings
% x and y wavenumbers 
qx = (-Nx/2:Nx/2-1)*2*pi/Lx;
qy = (-Ny/2:Ny/2-1)*2*pi/Ly;
% Fourier space derivative operators
Dx = 1i*repmat(qx,Ny,1);
Dy = 1i*repmat(qy',1,Nx);

D2  = Dx.^2 + Dy.^2;	% Laplacian

% invert the Laplacian (taking care of the central zero)
x_mid = Nx/2+1; % domain centers
y_mid = Ny/2+1; 
ID2   = 1./D2;  % invert Laplacian
ID2(y_mid,x_mid) = 0; % set the inverse's center to 0 rather than Inf

Dx(:,1) = zeros(Ny,1);
Dy(1,:) = zeros(1,Nx);

save('parameters.mat');
%edgar knobloch
%nathan katz

end
