function corr = correlation_get(filename)
load(filename, 'omega_t');
len = size(omega_t, 3);
corr = zeros(1, len);
for i = 1:len
  corr(i) = norm(omega_t(:,:,i) - omega_t(:,:,1), 'fro');
end

end