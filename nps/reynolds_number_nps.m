function [Re_avg, Re_test, Re_max] = reynolds_number_nps(filename)

%% Author: Namo
%% Parameters
% filename = name of the NPS matfile

% Calculate the average and maximum Re


%% Initialization

load(filename, 'U_t', 'V_t', 'nx', 'ny', 'Ls_mks', 'nu_mks','Ts_mks','Us_mks', ...
     'n0');

% Get the center velocities (due to grid being staggered)
[u_t, v_t] = get_center_vel(U_t, V_t, nx, ny);

% Temporary test value
Re_test = Us_mks * Ls_mks / nu_mks;

% Non-dimensional parameters
L0 = 1;
nu = nu_mks * Ts_mks / (Ls_mks^2);


%% Max Re from max velocity scale

vel = sqrt(u_t.^2 + v_t.^2);
maxVel = max(max(max(vel)));
Re_max = (L0 * maxVel) / nu;


%% Average Re

% Storing characteristic velocity at each snapshot
t = size(U_t, 3);
V = zeros(1, t);

for i = 1:t
    % crop out the edges because the velocity is zero there
    u_vel = u_t(n0+1:end-n0, n0+1:end-n0, i);
    v_vel = v_t(n0+1:end-n0, n0+1:end-n0, i);
    V(i) = sqrt((sum(sum(u_vel.^2 + v_vel.^2))) / ((nx-2*n0) * (ny-2*n0)));

end

V_avg = mean(V);
Re_avg = (L0 * V_avg) / nu;


end
