%% DESCRIPTION
% Get the U and V matrices corresponding to the center of each cell given
% the staggered velocity grids.
function [u_t, v_t] = get_center_vel(U_t, V_t, nx, ny)

%% INPUTS
% U_t, V_t :: Velocity fields in time
% nx, ny :: the desired dimensions of the outputs

% interpolation of u and v at center of each cell
[suy, sux, t] = size(U_t);
u_temp = zeros(suy, sux+2, t);
u_t = zeros(ny, nx, t);

[svy, svx, ~] = size(V_t);
v_temp = zeros(svy+2, svx, t);
v_t = zeros(ny, nx, t);

for i = 1:t
    u_temp(:, 2:end-1, i) = U_t(:,:,i); % u = 0 at boundaries
    u1 = u_temp(:, 1:end-1, i);
    u2 = u_temp(:, 2:end, i);
    u_t(:,:,i) = (u1 + u2)/2;

    v_temp(2:end-1, :, i) = V_t(:,:,i);
    v1 = v_temp(1:end-1, :, i);
    v2 = v_temp(2:end, :, i);
    v_t(:,:,i) = (v1 + v2)/2;
end

end
