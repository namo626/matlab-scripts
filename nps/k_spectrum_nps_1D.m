function [ks, energies] = k_spectrum_nps_1D(filename, binSize, dimension)

%% Author: Namo
%% Parameters
% filename  = name of the NPS matfile
% binSize   = bin size of the wavenumbers
% dimension = spectrum along 'x' or 'y'

% Output the wavenumber range and the corresponding energy
% distribution of the given flow field along the specified
% dimension.

% Example: [kx, e] = k_spectrum_nps_1D('file.mat', 0.1, 'x');


%% Initializing variables

load(filename, 'U_t', 'V_t', 'Ls_mks','n0','nx','ny')

% grid length (nondimensional)
gl = 1 / n0;
Nx = nx;
Ny = ny;

% Ngrid is the size of the square grid that will be made
if strcmp(dimension, 'x')
    Ngrid = Nx;
else
    Ngrid = Ny;
end

% gets the proper x and y velocity fields
[U_t, V_t] = get_center_vel(U_t, V_t, nx, ny);

% 1. For each flow snapshot, do FFT
% 2. Generate a grid of kx and ky values, with the origin at the center
% 3. Generate, from (2), a 2D grid with values as magnitude of (kx,ky) at
% each point (should be a circle)
% 4. For each range of magnitude, sum all the values from the fourier grid
% whose positions lie in that range
% 5. The result is a row vector containing energy at each interval, or
% "bin"
% 6. Find that energy vector for each snapshot, and time-average to get the
% final
if strcmp(dimension, 'kx')
    vel_t = U_t.^2;
else
    vel_t = V_t.^2;
end

t = size(vel_t, 3);
k_grid = row_k;


% initializing spectrum of first time snapshot
% all velocity values are ones in the center of each cell

% fourierU1 = fftshift(abs(fft2(U_t(:,:,1))));
% fourierV1 = fftshift(abs(fft2(V_t(:,:,1))));
%
% fourier1 = fourierU1.^2 + fourierV1.^2;
fourier1 = avgFourier(vel_t(:,:,1));
[energies1, k_int] = histogramize(k_grid, fourier1);

% 3D fourier domain array for each timestep
% fourier_t = zeros(size(fourier1, 1), size(fourier1, 2), t);
% fourier_t(:,:,1) = fourier1;

energies = zeros(t, length(energies1));
energies(1, :) = energies1;

for t_i = 2:t
  fourier_i = avgFourier(vel_t(:,:,t_i));
  energies(t_i,:) = histogramize(k_grid, fourier_i);
end

% energies = mean(energies);
ks = k_int;


%plot_k(ks, energies);

%-----------------------------------------------------------------------------
%% Functions

% calculates the 1D average of fourier spectrum along x or y direction
% input: a single frame of x or y velocity field (should be squared to
% represent energy)
    function fourier_vel = avgFourier(vel_snapshot)
        temp_fourier = zeros(size(vel_snapshot, 1), size(vel_snapshot, 2));
        if strcmp(dimension, 'kx')
            for i = 1:size(vel_snapshot, 1)
                temp_fourier(i,:) = fftshift(abs(fft(vel_snapshot(i,:))));
            end
            fourier_vel = mean(temp_fourier);
        else
            for i = 1:size(vel_snapshot, 2)
                temp_fourier(:,i) = fftshift(abs(fft(vel_snapshot(:,i))));
            end
            fourier_vel = mean(temp_fourier, 2);
        end
    end

  function [energies, k_int] = histogramize(k_grid, fourier_grid)
    % The limit of k is the maximum value in the radius grid
    k_max = max(k_grid);

    binNum = ceil(k_max / binSize);
    % The energy row vector
    energies = zeros(1, binNum);
    k_int = zeros(1, binNum);  % midpoint k values; will be used as x-axis
    for i = 1:binNum
      k1 = binSize*i;
      k2 = k1 + binSize;
      k_int(i) = k2;

      energies(i) = sum(sum(fourier_grid(k1 <= k_grid & k_grid < k2)));

    end
  end

  function grid = row_k
    % Generate only the first quadrant because of symmetry
%     [tx, ty] = meshgrid(1:Ngrid/2, Ngrid/2:-1:1);
    tx = 1:Ngrid/2;
    % Scale the grid into k wave numbers instead of merely grid positions
    % Factor is (1/grid length) * (1 / no. of gridpoints per axis) * 2pi
    factor = (1/gl) * (1/Ngrid) * (2*pi);
    tx = tx * factor;
    grid = [ fliplr(tx) tx ];

    % synchronizing grid size and actual box size
    % if they are the same, then this has no effect
%     if Nx > Ny
%         start = (nx - ny + 2) / 2;
%         finish = start + ny - 1;
%         grid = grid(start:finish, :);
%     else
%         start = (ny - nx + 2) / 2;
%         finish = start + nx - 1;
%         grid = grid(:, start:finish);
%     end
  end

%   function plot_k(ks, energies)
%     plot(ks, energies);
%     xlim([0, max(ks)]);
%     title(sprintf('Energy spectrum #%d, Nm = %d, Forcing = %.1f, L0 = %.3f, binWidth = %.2f', ind, Nm, amp, L0, binSize));
%     xlabel('$ \vert k \vert $', 'Interpreter', 'latex', 'FontSize', 14);
%     ylabel('Energy');
%   end

end
