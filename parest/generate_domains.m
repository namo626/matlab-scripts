%% DESCRIPTION
% This script repeatedly calls generate_domain with the same
% configuration and save each one with a different ID. This is done
% for statistical purpose.
function generate_domains(trials)

disp(sprintf('%d trials', trials));

tic;
for i = 1:trials
    generate_domain(i);
end

toc;
