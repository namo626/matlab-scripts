%%%% Integral Method Q2D-NS Parameter Estimation %%%%

%% PSEUDO-CODE

%{

INPUTS
------
filename : string for path to -v7.3 .mat file containing data
N_d : number of integration domains
N_h = [Q R S] : max number of harmonics of integration function, A, to sample
D = [fry, frx, Dt] : size of integration domain
if_track = 0/1 : track progress or not
sample_scheme = 'sp chunk'/'' : chunking or randomizing integration
domains
sampling_rate_xy = integer > 0 : spatial downsampling rate
sampling_rate_t = integer > 0 : temporal downsampling rate
deriv = a function handle : weight constructor

OUTPUTS
-------
(beta, nu alpha in physical units)
par : struct { beta, nu, alpha, D, time integration in seconds, N_d, dx}
Q
b

%}
%%
function [par,Q,b] = IntParEst(filename,N_d,N_h,D,if_track,sample_scheme, ...
                               sampling_rate_xy, sampling_rate_t, deriv)
%% INITIALIZE
% seed = 300; rng(seed);
% Define matfile
traj = matfile(filename);

% Load time-scale
Ts_mks = traj.Ts_mks;

% Get size of velocity fields using partial loading
% If U_t and V_t are staggered, interpolate them later
if all(size(traj.U_t(:,:,1)) ~= size(traj.V_t(:,:,1)))
    isStaggered = true;
    Ly_temp = traj.ny;
    Lx_temp = traj.nx;
    Lt = size(traj, 'U_t', 3);
else
    isStaggered = false;
    [Ly_temp,Lx_temp,Lt] = size(traj, 'U_t');
end

% Subsampling the spatial and temporal grids
Ly = floor((Ly_temp-1)/sampling_rate_xy) + 1;
Lx = floor((Lx_temp-1)/sampling_rate_xy) + 1;


% Grid densities
% This is the time between each frame of data (in seconds)
dt_mks = sampling_rate_t * (traj.tf_mks - traj.ti_mks) / Lt;
dx = traj.dx * sampling_rate_xy;

% Size of local domain
fry = D(1);
frx = D(2);
clearvars -global
global var
var.Dx = floor(frx*Lx); % size of inner domain
var.Dy = floor(fry*Ly);
var.Dt_temp = D(3);
var.Dt = floor((D(3)-1)/sampling_rate_t) + 1;

% Sampling scheme

if strcmp(sample_scheme,'sp chunk')

    n_chunks_x = 7;
    n_chunks_y = 9;

    var.Dx = floor(Lx/n_chunks_x);
    var.Dy = floor(Ly/n_chunks_y);

    px = 1:var.Dx:((n_chunks_x-1)*var.Dx + 1);
    py = 1:var.Dy:((n_chunks_y-1)*var.Dy + 1);

    [Px,Py] = meshgrid(px,py);

    N_d = n_chunks_x*n_chunks_y;

    P = zeros(3,N_d);
    P(1,:) = Px(:);
    P(2,:) = Py(:);
    P(3,:) = ones(1,N_d);

else

    disp('Using random spatial domains with chunking in time');

    P = zeros(3,N_d); % Starting corner of integration domain
    P(1,:) = randi([1,Lx-var.Dx+1],N_d,1);
    P(2,:) = randi([1,Ly-var.Dy+1],N_d,1);
    time_range = 1:var.Dt_temp:(Lt-var.Dt_temp+1);
    P(3,:) = time_range(randi([1,length(time_range)], N_d, 1));

end

% Define variable conversions
S_x = 2/(dx*var.Dx);
S_y = 2/(dx*var.Dy);
S_t = (2*Ts_mks)/(dt_mks*var.Dt);

% Create subdomain
var.x = linspace(-1,1,var.Dx);
var.y = linspace(-1,1,var.Dy);
var.t = linspace(-1,1,var.Dt);

% Initialize Target and Library
b = zeros(N_d*N_h(1)*N_h(2)*N_h(3),1);
Q = zeros(size(b,1), 3);

%% FILL TERMS

n_lib = 0;
n_track = 10;

disp(sprintf('Number of samples: %d', N_d*N_h(1)*N_h(2)*N_h(3)));

% weight function constructor of choice
%deriv = @derA_m;
% deriv = @derA_SCS;
disp(['Weight constructor: ' func2str(deriv) ]);

for np = 1:N_d

    % Indices for integration domain
    rx = P(1,np):(P(1,np)+var.Dx-1);
    ry = P(2,np):(P(2,np)+var.Dy-1);
    rt = P(3,np):(P(3,np)+var.Dt_temp-1);

    % Velocity fields on integration domain
    %             tic

    % Load the data partially
    % If grids are staggered, fix them (in space)
    if isStaggered
        [U_temp, V_temp] = get_center_vel(traj.U_t(:,:,rt), traj.V_t(:,:,rt), traj.nx, traj.ny);
        U_sub = U_temp(1:sampling_rate_xy:traj.ny, 1:sampling_rate_xy:traj.nx, :);
        V_sub = V_temp(1:sampling_rate_xy:traj.ny, 1:sampling_rate_xy:traj.nx, :);
        U = U_sub(ry,rx,1:sampling_rate_t:end);
        V = V_sub(ry,rx,1:sampling_rate_t:end);
    else
        U_sub = traj.U_t(1:sampling_rate_xy:Ly_temp, 1:sampling_rate_xy:Lx_temp, rt);
        V_sub = traj.V_t(1:sampling_rate_xy:Ly_temp, 1:sampling_rate_xy:Lx_temp, rt);
        U = U_sub(ry,rx,1:sampling_rate_t:end);
        V = V_sub(ry,rx,1:sampling_rate_t:end);
    end


    for q = 1:N_h(1)
        for r = 1:N_h(2)
            for s = 1:N_h(3)

            n_lib = n_lib + 1;
            if if_track
                fprintf('%d ', n_lib);
            end

            % if if_track && n_lib == n_track
            %     if n_lib < 100
            %         disp(['Library Row # : ',num2str(n_lib)])
            %         n_track = n_track + 10;
            %     elseif n_lib < 1000
            %         disp(['Library Row # : ',num2str(n_lib)])
            %         n_track = n_track + 100;
            %     else
            %         disp(['Library Row # : ',num2str(n_lib)])
            %         n_track = n_track + 1000;
            %     end
            % end

            % Make wave numbers global to be use in deriveA()
            var.q = q;
            var.r = r;
            var.s = s;

            fx = 0.5;
            fy = 0.5;
            ft = 0.5;
            % % Pre-make derivatives of windowing functions
            % dA101 = deriv([1,0,1],[fx,fy,ft]); dA011 = deriv([0,1,1]
            % dA020 = deriv(0,2,0); dA200 = deriv(2,0,0); dA110 = deriv(1,1,0);
            % dA210 = deriv(2,1,0); dA030 = deriv(0,3,0); dA300 = deriv(3,0,0); dA120 = deriv(1,2,0);
            % dA100 = deriv(1,0,0); dA010 = deriv(0,1,0);
            % % ---> the 3 digits correspond here to derivatives NOT to harmonics
            dA100 = deriv([1,0,0], [fx,fy,ft]);
            dA010 = deriv([0,1,0], [fx,fy,ft]);
            dA101 = deriv([1,0,1], [fx,fy,ft]);
            dA110 = deriv([1,1,0], [fx,fy,ft]);
            dA011 = deriv([0,1,1], [fx,fy,ft]);
            dA200 = deriv([2,0,0], [fx,fy,ft]);
            dA020 = deriv([0,2,0], [fx,fy,ft]);
            dA210 = deriv([2,1,0], [fx,fy,ft]);
            dA120 = deriv([1,2,0], [fx,fy,ft]);
            dA300 = deriv([3,0,0], [fx,fy,ft]);
            dA030 = deriv([0,3,0], [fx,fy,ft]);

            % Daniel's function
            % dA101 = derA_daniel([1 0 1]);
            % dA011 = derA_daniel([0 1 1]);
            % dA020 = derA_daniel([0 2 0]);
            % dA200 = derA_daniel([2 0 0]);
            % dA110 = derA_daniel([1 1 0]);
            % dA210 = derA_daniel([2 1 0]);
            % dA030 = derA_daniel([0 3 0]);
            % dA300 = derA_daniel([3 0 0]);
            % dA120 = derA_daniel([1 2 0]);
            % dA100 = derA_daniel([1 0 0]);
            % dA010 = derA_daniel([0 1 0]);

            % size(dA101)
            % size(U)

            % Target
            B = V.*dA101*S_x*S_t - U.*dA011*S_y*S_t;
            b(n_lib,1) = trapz(var.x,trapz(var.y,trapz(var.t,B,3)));

            % Advection Term (incompressible)
            th1 = U.*V.*(dA020*S_y^2 - dA200*S_x^2) + ...
                  (U.^2 - V.^2).*dA110*S_x*S_y;
            Q(n_lib,1) = trapz(var.x,trapz(var.y,trapz(var.t,th1,3)));

            % Laplacian Term
            th2 = U.*(dA210*S_x^2*S_y + dA030*S_y^3) - ...
                V.*(dA300*S_x^3 + dA120*S_x*S_y^2);
            Q(n_lib,2) = trapz(var.x,trapz(var.y,trapz(var.t,th2,3)));

            % *Simple* Rayleigh Term
            th3 = V.*dA100*S_x - U.*dA010*S_y;
            Q(n_lib,3) = trapz(var.x,trapz(var.y,trapz(var.t,th3,3)));


            end
        end
    end

end


%% REGRESSION

% Parameters

% ksi = SINDy(Theta, b); % sparsify library
ksi = Q \ b;

% How good was the estimation
% res = norm(b-[Th1,Th2,Th3]*ksi);


% Convert to physical parameters
beta = ksi(1); % non-dim
nu_mks = traj.Us_mks*traj.Ls_mks*ksi(2); %m^2/s
alpha_mks = traj.Us_mks*ksi(3)/traj.Ls_mks; %s^-1

par.beta = beta;
par.nu = nu_mks;
par.alpha = alpha_mks;
par.domain = D;
par.N = N_d;
par.T = dt_mks * var.Dt;
par.dx = dx;
par.weight = func2str(deriv);

%% REMINDERS


end
%% --------------------------------------------------------------- SINDy()
function Xi = SINDy (Theta, dXdt)

% compute sparse regression on dX = Theta * Xi
% regression technique used: sequential least squares
% code taken directly from Supporting Info for SINDy Paper

    Xi = Theta \ dXdt;

    ref = mean(abs(dXdt));

    lambda = mean(abs(Theta*Xi - dXdt))/ref
%     lambda = 0.05*mean(abs(dXdt)) % threshold to determine term as "small"

    nrm0 = mean(abs(dXdt))/ref;
    nrm1 = mean(abs(Xi(1)*Theta(:,1)))/ref;
    nrm2 = mean(abs(Xi(2)*Theta(:,2)))/ref;
    nrm3 = mean(abs(Xi(3)*Theta(:,3)))/ref;
%     nrm4 = mean(abs(Xi(4)*Theta(:,4)))/ref;

    R0 = nrm0/lambda;
    R1 = nrm1/lambda;
    R2 = nrm2/lambda;
    R3 = nrm3/lambda;
%     R4 = nrm4/lambda;

    for i = 1:3

      product = zeros(size(Xi)); % product of the coefficient and characteristic size of library function
      [~,w] = size(Theta);
      for p_ind = 1:w
        product(p_ind) = mean(abs(Xi(p_ind)*Theta(:,p_ind)))/ref;
      end

      smallinds = (product < lambda);
      Xi(smallinds) = 0;    % set negligible terms to 0
      for ind = 1:size(dXdt,2)   % perform regression on each vector independently
        biginds = ~smallinds(:,ind);
        Xi(biginds,ind) = Theta(:,biginds) \ dXdt(:,ind);

        lambda = mean(abs(Theta(:,biginds)*Xi(biginds,ind)-dXdt(:,ind)))/ref;

      end
    end

lambda

end
