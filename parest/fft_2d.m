%% DESCRIPTION
% Computes the average fft along an axis of a given 2D array. Also
% calculates the frequency axis and the first peak frequency.
function [fpeak, F] = fft_2d(arr, Fs, N, ax)

A = arr - mean(arr, ax);
F = abs(mean(fft(A, N, ax), 3-ax));
freq = (0:(N-1)) * (Fs/N);
[~, argmax] = max(F);
fpeak = freq(argmax);

end
