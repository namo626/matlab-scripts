%% DESCRIPTION
% This function plots a scatter of the error of key parameters beta,
% nu, and alpha against a variable of the algorithm, e.g. number of
% integration domains

%% SPECIFICATION
% data = struct { betas, nus, alphas, std_betas, std_nus, std_alphas, N, domain, T }

function plotError_std(data, variable)

e1 = 100*data.betas;
e2 = 100*data.nus;
e3 = 100*data.alphas;
std1 = 100*data.std_betas;
std2 = 100*data.std_nus;
std3 = 100*data.std_alphas;

domains = data.N;
%x = domains;
x = data.fx;
% x = data.samplings;

%T = data.T;
T = 100;

clf
hold on
errorbar(x, e1, std1);
errorbar(x, e2, std2);
errorbar(x, e3, std3);

w = data.weight;
if strcmp(w, 'derA_SCS')
    wtt = ['weight = (x^{2}-1)^{3} sin(\pix) cos(\pix) (y^{2}-1)^{3} ' ...
           '' 'sin(\piy) sin(\pit)'];
elseif strcmp(w, 'Multiple weights')
    wtt = 'Multiple weights based on peak frequencies';
    wtt = '';
else
    wtt = ['weight = (x^{2}-1)^{3} sin(\pix) (y^{2}-1)^{3} ' ...
           '' 'sin(\piy) sin(\pit)'];
end

dm = sprintf('Domain Size: %.2f, %.2f, %d s', data.domain(1), ...
             data.domain(2), T);
legend('beta', 'nu', 'alpha');
title({['Relative Error vs. ' variable],...
      dm, wtt})
xlabel(variable)
ylabel('Percent Relative Error (%)')



end
