function Xi = SINDy (Theta, dXdt)

% compute sparse regression on dX = Theta * Xi
% regression technique used: sequential least squares
% code taken directly from Supporting Info for SINDy Paper

    Xi = Theta \ dXdt;

    ref = mean(abs(dXdt));

    lambda = mean(abs(Theta*Xi - dXdt))/ref;
%     lambda = 0.05*mean(abs(dXdt)) % threshold to determine term as "small"

    nrm0 = mean(abs(dXdt))/ref;
    nrm1 = mean(abs(Xi(1)*Theta(:,1)))/ref;
    nrm2 = mean(abs(Xi(2)*Theta(:,2)))/ref;
    nrm3 = mean(abs(Xi(3)*Theta(:,3)))/ref;
%     nrm4 = mean(abs(Xi(4)*Theta(:,4)))/ref;

    R0 = nrm0/lambda;
    R1 = nrm1/lambda;
    R2 = nrm2/lambda;
    R3 = nrm3/lambda;
%     R4 = nrm4/lambda;

    for i = 1:3

      product = zeros(size(Xi)); % product of the coefficient and characteristic size of library function
      [~,w] = size(Theta);
      for p_ind = 1:w
        product(p_ind) = mean(abs(Xi(p_ind)*Theta(:,p_ind)))/ref;
      end

      smallinds = (product < lambda);
      Xi(smallinds) = 0;    % set negligible terms to 0
      for ind = 1:size(dXdt,2)   % perform regression on each vector independently
        biginds = ~smallinds(:,ind);
        Xi(biginds,ind) = Theta(:,biginds) \ dXdt(:,ind);

        lambda = mean(abs(Theta(:,biginds)*Xi(biginds,ind)-dXdt(:,ind)))/ref;

      end
    end


end