%%%% Barkley Weak Formulation System Identification %%%%%
function [ksi,res,Qu,Qv] = ParEst_WF_Barkley(filename,N_d,N_h,D,if_track,sig)
%{
INPUTS:
-------
filename                -- string containing data
N_d                     -- number of integration domains to sample per freq
N_h = [hx,ht]           -- number of weights in each dimension to sample
D = [Dx,Dt]             -- size of integration domain in space/time
if_track                -- bool for showing progress in command window
sig                     -- magnitude of noise to apply before estimation

OUTPUTS:
--------
ksi     -- non dimensional estimated parameters
res     -- residual = mean(abs(Q*ksi))
Qu,Qv   -- Assembled library
%}

%% INITIALIZE
% Define matfile
traj = matfile(filename);

% Get size of velocity fields
[~,Lx,Lt] = size(traj,'U_t');

% Grid densities
dt = traj.dt;
dx = traj.dx; 

% Size of local domain
clearvars -global
global var
var.Dx = D(1);
var.Dt = D(2);

var.harX = N_h(1);
var.harT = N_h(2);

% Create subdomain
var.x = linspace(-1,1,var.Dx+1);
var.t = linspace(-1,1,var.Dt+1);

% Margins from the recording edges
xmargin = 0;
tmargin = 0;

% Define variable conversions
S_x = 2/(dx*var.Dx);
S_t = 2/(dt*var.Dt);

P = zeros(3,1);

% Initialize Target and Library
Qu = zeros(N_d*var.harX^2*var.harT,11);
Qv = zeros(N_d*var.harX^2*var.harT,8);

%% FILL TERMS

n_lib = 0;
n_track = 10;

U_full = traj.U_t;
V_full = traj.V_t;

% Add noise to fields
if sig > 0
    U_full = U_full + sig*std(U_full(:))*randn(size(U_full));
    V_full = V_full + sig*std(V_full(:))*randn(size(V_full));
end

% Pre-make orthogonal polynomials
for ox = 0:var.harX-1
for oy = 0:var.harX-1
for ot = 0:var.harT-1
    dA000 = weight_full([0,0,0],[ox,oy,ot]);
    dA001 = weight_full([0,0,1],[ox,oy,ot]);
    dA200 = weight_full([2,0,0],[ox,oy,ot]);
    dA020 = weight_full([0,2,0],[ox,oy,ot]);
    dA002 = weight_full([0,0,2],[ox,oy,ot]);
for np = 1:N_d  
    n_lib = n_lib + 1;

    if if_track && n_lib == n_track
        if n_lib < 100
            disp(['Library Row # : ',num2str(n_lib)])
            n_track = n_track + 10;
        elseif n_lib < 1000
            disp(['Library Row # : ',num2str(n_lib)])
            n_track = n_track + 100;
        else
            disp(['Library Row # : ',num2str(n_lib)])
            n_track = n_track + 1000;
        end
    end

    % Make sure fields exist in integration domain
    if_GoodDomain = 0;
    while if_GoodDomain < 1
        % Choose random point
        P(1) = randi([1+xmargin,Lx-var.Dx-xmargin]);
        P(2) = randi([1+xmargin,Lx-var.Dx-xmargin]);
        P(3) = randi([1+tmargin,Lt-var.Dt-tmargin]);

        % Indices for integration domain
        rx = P(1):(P(1) + var.Dx);
        ry = P(2):(P(2) + var.Dx);
        rt = P(3):(P(3) + var.Dt);

        % U in integration domain
        U = U_full(ry,rx,rt); 

        if mean(U(:))>0.025 % arbitrary threshold
            if_GoodDomain = 1;
        end

    end

    % Pull V as well
    V = V_full(ry,rx,rt);

    %%%%% U %%%%%
    % du/dt (LHS: 1)
    B = -U.*dA001*S_t;
    Qu(n_lib,1) = trapz(var.x,trapz(var.x,trapz(var.t,B,3),2),1);
    % \nabla^2 u (RHS: D=64)
    th2 = U.*(dA020 + dA200)*S_x^2;
    Qu(n_lib,2) = trapz(var.x,trapz(var.x,trapz(var.t,th2,3),2),1);

    % u (-b/a/eps=-4.1667)
    th3 = U.*dA000;
    Qu(n_lib,3) = trapz(var.x,trapz(var.x,trapz(var.t,th3,3),2),1);

    % u^2 ((1+b/a)/eps=54.1667)
    th4 = U.^2.*dA000;
    Qu(n_lib,4) = trapz(var.x,trapz(var.x,trapz(var.t,th4,3),2),1);

    % u^3 (-1/eps = -50)
    th5 = U.^3.*dA000;
    Qu(n_lib,5) = trapz(var.x,trapz(var.x,trapz(var.t,th5,3),2),1);

    % uv (-1/a/eps = -83.33)
    th6 = U.*V.*dA000;
    Qu(n_lib,6) = trapz(var.x,trapz(var.x,trapz(var.t,th6,3),2),1);

    % u^2v (1/a/eps = 83.33)
    th7 = U.^2.*V.*dA000;
    Qu(n_lib,7) = trapz(var.x,trapz(var.x,trapz(var.t,th7,3),2),1);

    % d^2u/dt^2 (0)
    th8 = U.*dA002*S_t^2;
    Qu(n_lib,8) = trapz(var.x,trapz(var.x,trapz(var.t,th8,3),2),1);

    % v^2 (0)
    th9 = V.^2.*dA000;
    Qu(n_lib,9) = trapz(var.x,trapz(var.x,trapz(var.t,th9,3),2),1);

    % uv^2 (0)
    th10 = U.*V.^2.*dA000;
    Qu(n_lib,10) = trapz(var.x,trapz(var.x,trapz(var.t,th10,3),2),1);

    % constant
    th11 = U.^0;
    Qu(n_lib,11) = trapz(var.x,trapz(var.x,trapz(var.t,th11,3),2),1);

    %%%%% V %%%%%
    % dv/dt
    B = -V.*dA001*S_t;
    Qv(n_lib,1) = trapz(var.x,trapz(var.x,trapz(var.t,B,3),2),1);

    % u (1)
    th2 = U.*dA000;
    Qv(n_lib,2) = trapz(var.x,trapz(var.x,trapz(var.t,th2,3),2),1);

    % v (-1)
    th3 = V.*dA000;
    Qv(n_lib,3) = trapz(var.x,trapz(var.x,trapz(var.t,th3,3),2),1);

    % u^2 (0)
    th4 = U.^2.*dA000;
    Qv(n_lib,4) = trapz(var.x,trapz(var.x,trapz(var.t,th4,3),2),1);

    % v^2 (0)
    th5 = V.^2.*dA000;
    Qv(n_lib,5) = trapz(var.x,trapz(var.x,trapz(var.t,th5,3),2),1);

    % u*v (0)
    th6 = U.*V.*dA000;
    Qv(n_lib,6) = trapz(var.x,trapz(var.x,trapz(var.t,th6,3),2),1);

    % \nabla^2 u (0)
    th7 = V.*(dA020 + dA200)*S_x^2;
    Qv(n_lib,7) = trapz(var.x,trapz(var.x,trapz(var.t,th7,3),2),1);

    % constant
    th8 = V.^0;
    Qv(n_lib,8) = trapz(var.x,trapz(var.x,trapz(var.t,th8,3),2),1);

end % np
end % harmonics
end
end

%% REGRESSION

% Parameters
ksi.u = SINDy2(Qu(:,:));
ksi.v = SINDy2(Qv(:,:));

if ksi.u(1)~=0
    ksi.u = ksi.u/ksi.u(1);
end
if ksi.v(1)~=0
    ksi.v = ksi.v/ksi.v(1);
end

% How good was the estimation
res_u = norm(Qu(:,:)*ksi.u)/norm(Qu(:,:))/norm(ksi.u);
res_v = norm(Qv(:,:)*ksi.v)/norm(Qv(:,:))/norm(ksi.v);

res = [res_u, res_v];

% Find "physical" parameters
D = ksi.u(2);
eps = -1/ksi.u(5);
a = ksi.u(5)*(1/ksi.u(6)-1/ksi.u(7))/2;
b = (ksi.u(3)-ksi.u(5)-ksi.u(4))*(1/ksi.u(6)-1/ksi.u(7))/4;

%% REMINDERS

disp(' ')
disp(' ')

end
%% Assorted functions
function W = weight_full(k,o)
global var
%{
Assemble the 1D weight functions into the full weight

k = [kx,ky,kt]: order of derivative(s)
w = [ox,oy,ot]: related to choice of sin/cos and frequency
%}
s = pi/2;
ifsinx = mod(o(1),2);
ifsiny = mod(o(2),2);
ifsint = mod(o(3),2);
o1 = s*floor((o(1)+1)/2);
o2 = s*floor((o(2)+1)/2);
o3 = s*floor((o(3)+1)/2);
for i=0:k(1)
    if k(1)==0
        coeff = 1;
    else
        coeff = nchoosek(k(1),i);
    end
    wxi(:,i+1) = coeff*weight_poly(var.x,3,k(1)-i);
    sign = (-1)^(floor((i+1-ifsinx)/2));
    if mod(i+ifsinx,2)==0
       trxi(:,i+1)=sign*o1^i*cos(o1*var.x);
    else
       trxi(:,i+1)=sign*o1^i*sin(o1*var.x); 
    end
end
for i=0:k(2)
    if k(2)==0
        coeff = 1;
    else
        coeff = nchoosek(k(2),i);
    end
    wyi(:,i+1) = coeff*weight_poly(var.x,3,k(2)-i);
    sign = (-1)^(floor((i+1-ifsiny)/2));
    if mod(i+ifsiny,2)==0
       tryi(:,i+1)=sign*o2^i*cos(o2*var.x);
    else
       tryi(:,i+1)=sign*o2^i*sin(o2*var.x); 
    end
end
for i=0:k(3)
    if k(3)==0
        coeff = 1;
    else
        coeff = nchoosek(k(3),i);
    end
    wti(:,i+1) = coeff*weight_poly(var.t,3,k(3)-i);
    sign = (-1)^(floor((i+1-ifsint)/2));
    if mod(i+ifsint,2)==0
       trti(:,i+1)=sign*o3^i*cos(o3*var.t);
    else
       trti(:,i+1)=sign*o3^i*sin(o3*var.t); 
    end
end

wx = sum(wxi.*trxi,2);
wy = sum(wyi.*tryi,2);
wt = sum(wti.*trti,2);

[wX,wY,wT] = meshgrid(wx,wy,wt);

W = wX.*wY.*wT;

end
function p = weight_poly(x,m,k)
%{
Polynomial piece of weighting function used to satisfy BC

A = d^k/dx^k[ (x^2 - 1)^m ]

x: independent variable
m: power of base function
k: order of derivative
%}

a = zeros(m*2 + 1,1); % initial coefficent vector
for l = 0:m
    a(2*l+1) = (-1)^(m-l)*nchoosek(m,l); % set polynomial coefficients
end 

c = zeros(2*m+1,1); % final coefficient vector
for n = 0:(2*m - k)
    c(n+1) = a(n+1+k)*factorial(n+k)/factorial(n);
end

p = 0;
for n = 0:(2*m-k)
    p = p + c(n+1)*x.^n; % final windowing function
end

end
