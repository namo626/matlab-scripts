%% DESCRIPTION
% Compute the relative error of x with true value t
% x can be a vector

function err = relError(x, t)

err = abs(x - t) ./ t;

end