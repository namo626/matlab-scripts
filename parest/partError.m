function partError(par)

alpha = 0.065;
beta = 0.826;
nu = 3.23e-6;

partitions = size(par.beta,2);

% scatter(1:partitions, rel(par.beta, beta), 200, 'filled');
% hold on;
% scatter(1:partitions, rel(par.alpha, alpha), 200, 'filled','d');
% hold on;
% scatter(1:partitions, rel(par.nu, nu), 200, 'filled','s');
% hold off

y = zeros(partitions, 3);
y(:,1) = rel(par.beta, beta);
y(:,2)= rel(par.nu, nu);
y(:,3) = rel(par.alpha, alpha);

figure
bar(y);

xlabel('Partition Number', 'FontSize', 17)
ylim([0 0.2]);
ylabel('Relative Error', 'FontSize', 17)
% xticks(1:partitions);
legend('Beta', 'Nu', 'Alpha');
ax = gca;
ax.FontSize = 20;


function err = rel(x, t)

err = abs(x-t)/t;

end

end
