%% DESCRIPTION
% Construct the derivatives of a weight of the form sin(q*2pi*z)
function wc = weight_S(z, ord, q)

%% INPUTS
% z : vector of the independent variable
% q : frequency of the sine term
% ord : order of derivative

%% Differentiate the whole weight
if ord == 0
    wc = sin(q*2*pi*z);
elseif ord == 1
    wc = 2*q*pi*cos(q*2*pi*z);
else
    error('Max derivative order is 3.');
end