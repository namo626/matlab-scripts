%%% Multiplicative A %%%
function dA = derA_SCS_test(L,M,N)
%{


%}

global var

% Create subdomain
x = var.x;
y = var.y;
t = var.t;
[X,Y,T] = meshgrid(x,y,t);

% Normalization Used in All Derivatives
[hx,~,~] = meshgrid(h_m(x,0),y,t);
[hx1,~,~] = meshgrid(h_m(x,1),y,t); % first derivative of h
[hx2,~,~] = meshgrid(h_m(x,2),y,t); % second derivative of h
[hx3,~,~] = meshgrid(h_m(x,3),y,t); % third derivative of h

[~,hy,~] = meshgrid(x,h_m(y,0),t);
[~,hy1,~] = meshgrid(x,h_m(y,1),t); % first derivative of h
[~,hy2,~] = meshgrid(x,h_m(y,2),t); % second derivative of h
[~,hy3,~] = meshgrid(x,h_m(y,3),t); % third derivative of h

q = var.q;

if L == 0
    Ax = 0.5*hx.*sin(2*pi*q*X);
elseif L == 1
    Ax = 0.5*hx1.*sin(2*pi*q*X) + pi*q*hx.*cos(2*pi*q*X);
elseif L == 2
    Ax = 0.5*hx2.*sin(2*pi*q*X) + 2*pi*q*hx1.*cos(2*pi*q*X) - ...
         2*(pi*q)^2*hx.*sin(2*pi*q*X);
elseif L == 3
    Ax = 0.5*hx3.*sin(2*pi*q*X) + 3*pi*q*hx2.*cos(2*pi*q*X) - ...
         6*(pi*q)^2*hx1.*sin(2*pi*q*X) - ...
         4*(pi*q)^3*hx.*cos(2*pi*q*X);
end

if M == 0
    Ay = sin(var.r*pi*Y).*hy;
elseif M == 1
    Ay = hy1.*sin(var.r*pi*Y) + (var.r*pi)*cos(var.r*pi*Y).*hy;
elseif M == 2
    Ay = hy2.*sin(var.r*pi*Y) + 2*var.r*pi*hy1.*cos(var.r*pi*Y) - ...
        (var.r*pi)^2*sin(var.r*pi*Y).*hy;
elseif M == 3
    Ay = hy3.*sin(var.r*pi*Y) + 3*var.r*pi*hy2.*cos(var.r*pi*Y) - ...
        3*(var.r*pi)^2*hy1.*sin(var.r*pi*Y) - (var.r*pi)^3*cos(var.r*pi*Y).*hy;
end

if N == 0
    At = sin(var.s*pi*T);
elseif N == 1
    At = var.s*pi*cos(var.s*pi*T);
end

dA = Ax.*Ay.*At;


end