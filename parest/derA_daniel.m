function W = derA_daniel(kx,ky,kt)
global var
%{
Assemble the 1D weight functions into the full weight

k = [kx,ky,kt]: order of derivative(s)
w = [ox,oy,ot]: related to choice of sin/cos and frequency
%}

o = [var.q var.r var.s];
k = [kx ky kt];

s = pi/2;
ifsinx = mod(o(1),2);
ifsiny = mod(o(2),2);
ifsint = mod(o(3),2);
o1 = s*floor((o(1)+1)/2);
o2 = s*floor((o(2)+1)/2);
o3 = s*floor((o(3)+1)/2);
for i=0:k(1)
    if k(1)==0
        coeff = 1;
    else
        coeff = nchoosek(k(1),i);
    end
    wxi(:,i+1) = coeff*weight_poly(var.x,3,k(1)-i);
    sign = (-1)^(floor((i+1-ifsinx)/2));
    if mod(i+ifsinx,2)==0
       trxi(:,i+1)=sign*o1^i*cos(o1*var.x);
    else
       trxi(:,i+1)=sign*o1^i*sin(o1*var.x);
    end
end
for i=0:k(2)
    if k(2)==0
        coeff = 1;
    else
        coeff = nchoosek(k(2),i);
    end
    wyi(:,i+1) = coeff*weight_poly(var.y,3,k(2)-i);
    sign = (-1)^(floor((i+1-ifsiny)/2));
    if mod(i+ifsiny,2)==0
       tryi(:,i+1)=sign*o2^i*cos(o2*var.y);
    else
       tryi(:,i+1)=sign*o2^i*sin(o2*var.y);
    end
end
for i=0:k(3)
    if k(3)==0
        coeff = 1;
    else
        coeff = nchoosek(k(3),i);
    end
    wti(:,i+1) = coeff*weight_poly(var.t,3,k(3)-i);
    sign = (-1)^(floor((i+1-ifsint)/2));
    if mod(i+ifsint,2)==0
       trti(:,i+1)=sign*o3^i*cos(o3*var.t);
    else
       trti(:,i+1)=sign*o3^i*sin(o3*var.t);
    end
end

wx = sum(wxi.*trxi,2);
wy = sum(wyi.*tryi,2);
wt = sum(wti.*trti,2);

[wX,wY,wT] = meshgrid(wx,wy,wt);

W = wX.*wY.*wT;

end
function p = weight_poly(x,m,k)
%{
Polynomial piece of weighting function used to satisfy BC

A = d^k/dx^k[ (x^2 - 1)^m ]

x: independent variable
m: power of base function
k: order of derivative
%}

a = zeros(m*2 + 1,1); % initial coefficent vector
for l = 0:m
    a(2*l+1) = (-1)^(m-l)*nchoosek(m,l); % set polynomial coefficients
end

c = zeros(2*m+1,1); % final coefficient vector
for n = 0:(2*m - k)
    c(n+1) = a(n+1+k)*factorial(n+k)/factorial(n);
end

p = 0;
for n = 0:(2*m-k)
    p = p + c(n+1)*x.^n; % final windowing function
end

end