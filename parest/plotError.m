%% DESCRIPTION
% This function plots a scatter of the error of key parameters beta,
% nu, and alpha against a variable of the algorithm, e.g. number of
% integration domains

%% SPECIFICATION
% data = struct { betas, nus, alphas, N, domain, T }

function plotError(data, variable)

alpha = 0.065;
beta = 0.826;
nu = 3.23e-6;

e1 = 100*relError(data.betas, beta);
e2 = 100*relError(data.nus, nu);
e3 = 100*relError(data.alphas, alpha);

x = data.N;
% x = data.sampling;

clf
hold on
plot(x, e1);
plot(x, e2);
plot(x, e3);

dm = sprintf('Domain Size: %.2f, %.2f, %d s', data.domain(1), ...
             data.domain(2), data.T);
legend('beta', 'nu', 'alpha');
title({['Relative Error vs. ' variable],...
      dm})
xlabel(variable)
ylabel('Percent Relative Error (%)')



end
