function Xi = SINDy2Clean (Theta)

% compute sparse regression on 0 = Theta * Xi
% first column is (usually) dx/dt term
% regression technique used: sequential least squares
% code rewritten from Supporting Info for SINDy Paper
    M = 1000;
    gamma = 2; % >1
    delta = 1e-6;
    [h,w] = size(Theta);
    LHS = [zeros(h,1); M];
    for i=1:w
        norms(i) = norm(Theta(:,i));
        Theta(:,i) = Theta(:,i)/norms(i);
    end

    ThetaA = [Theta; M*(1+0.1*rand(1,w))/w];
    Xi = ThetaA \ LHS;


    lambda = norm(ThetaA*Xi-LHS); % residual as threshold
    smallinds = zeros(w,1);
    for i=1:1e5
          % product of the coefficient and characteristic size of library function
          for p_ind = 1:w
            product(p_ind) = mean(abs(Xi(p_ind)*Theta(:,p_ind)));
          end
          product(smallinds==1)=Inf;
          [Y,I] = min(product);
          smallinds(I) = 1;
          Xi_old = Xi;
          Xi(smallinds==1) = 0;    % set negligible terms to 0
          Xi(smallinds==0) = ThetaA(:,smallinds==0) \ LHS(:);
          lambda_old = lambda;
          lambda = norm(ThetaA*Xi-LHS);
          if (lambda/lambda_old > gamma && lambda>delta) || nnz(Xi)==0
             Xi = Xi_old./norms';
             break
          end
    end
end