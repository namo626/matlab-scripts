%% DESCRIPTION
% This script plots the relative error of given average values and
% standard deviations.
function plotError2(data)

alpha = 0.065;
beta = 0.826;
nu = 3.23e-6;

e1 = 100*relError(data.betas, beta);
e2 = 100*relError(data.nus, nu);
e3 = 100*relError(data.alphas, alpha);

u1 = 100*(data.std_betas/beta);
u2 = 100*(data.std_nus/nu);
u3 = 100*(data.std_alphas/alpha);

x = data.frx;

clf;
hold on;
errorbar(x, e1, u1);
errorbar(x, e2, u2);
errorbar(x, e3, u3);
title(sprintf('Rel Error vs. domain size in x (Dy = %d%%)', ...
              round(data.fry*100)));
xlabel('Domain size in x');
ylabel('Percent relative error');