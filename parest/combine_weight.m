%% DESCRIPTION
% Combines the full 3D weight using the specified constructors for
% x and y parts. Time part defaults to sin(s*pi*t).
function W = combine_weight(fx, fy, ft, derivs, freqs)

global var;

x = var.x; y = var.y; t = var.t;
q = freqs(1); r = freqs(2); s = freqs(3);
L = derivs(1); M = derivs(2); N = derivs(3);

wx = fx(x, L, q);
wy = fy(y, M, r);
wt = ft(t, N, s);

% if N == 0
%     wt = sin(s*2*pi*t);
% elseif N == 1
%     wt = 2*s*pi*cos(2*s*pi*t);
% end

[Wx, Wy, Wt] = meshgrid(wx,wy,wt);
W = Wx.*Wy.*Wt;
