% This script plots the variation of error over different
% combinations of domain sizes (in x and y)
function size_dependence(data)

%% Find the norm of the errors
alpha = 0.065;
beta = 0.826;
nu = 3.23e-6;

e1 = relError(data.betas, beta);
e2 = relError(data.nus, nu);
e3 = relError(data.alphas, alpha);

E = 100*(max(cat(3,e1,e2,e3), [],3));

%% Plot the colormap
[X,Y] = meshgrid(data.frx, data.fry);

pcolor(X,Y,E);
colorbar

end
