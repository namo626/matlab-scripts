%%%% Integral Method Q2D-NS Parameter Estimation %%%%
%% This applies the method to different (spatial) regions in the
%% flow field to determine parameter localizations
%% This is specific to experimental data (Kolmogorov)

%% PSEUDO-CODE

%{

INPUTS
------
filename : string for path to -v7.3 .mat file containing data
N_d : number of integration domains
N_h = [Q R S] : max number of harmonics of integration function, A, to sample
D = [fr, Dt] : size of integration domain
num_rows : number of rows to partition
num_cols : number of columns to partition

OUTPUTS
-------
par : estimated parameters in physical units
ksi : estimated parameters in non-dimensinoal units
res : residual of estimation (utility unclear)

Initialize
    -size of local domain
    -how many harmonics to sample
    -any tracking variables
    -library and target

Fill terms
    -advection term
    -laplacian term
    -rayleigh term
    -time-derivative term

Regression
    -invert library onto target
    -calculate norm of b-\Theta\xi

Reminders
    -disp to-do list

deriveA(L,M,N)
    -inputs: derivative orders
    -output: 3D array corresponding to local sub-domain

h(x,k,q)
    -inputs: absiscae, derivative order, harmonic of sinusoid
    -output: additive windowing polynomial

%}
%%
function [par,Q,b] = IntParEst_local_exp(filename,N_d,N_h,D,num_rows, ...
                                         num_cols,if_track,sample_scheme)
seed = 1; rng(seed);

%% INITIALIZE
% Define matfile
traj = matfile(filename);
U_t = traj.U_t;
V_t = traj.V_t;
X_nd = traj.X_nd;

% Physical scales
Ls_mks = 0.0127;
Us_mks = 0.0058;
Ts_mks = 2.2006;

% Get size of velocity fields
[Ly,Lx,Lt] = size(U_t);

% Grid densities
dx = X_nd(1,2) - X_nd(1,1);
dt_mks = traj.dt;

% Partition the spatial regions
row_size = floor(Ly/num_rows);
col_size = floor(Lx/num_rows);

% Size of local domain
fr = D(1);
clearvars -global
global var
var.Dx = round(fr*Lx); % size of inner domain
var.Dy = round(fr*Lx);
var.Dt = D(2);

if (var.Dx > col_size) | (var.Dy > row_size)
    error(['Integration domain must be smaller than partition ' ...
           'size']);
end

% Sampling scheme

if strcmp(sample_scheme,'sp chunk')
    disp('Using chunking for integration domain');

    n_chunks_x = 7;
    n_chunks_y = 8;

    var.Dx = floor(col_size/n_chunks_x);
    var.Dy = floor(row_size/n_chunks_y);

    px = 1:var.Dx:((n_chunks_x-1)*var.Dx + 1);
    py = 1:var.Dy:((n_chunks_y-1)*var.Dy + 1);

    [Px,Py] = meshgrid(px,py);

    N_d = n_chunks_x*n_chunks_y;

    P = zeros(3,N_d);
    P(1,:) = Px(:);
    P(2,:) = Py(:);
    P(3,:) = ones(1,N_d);

else

    P = zeros(3,N_d); % Starting corner of integration domain
    P(1,:) = randi([1,Lx-var.Dx],N_d,1);
    P(2,:) = randi([1,Ly-var.Dy],N_d,1);
    % P(3,:) = randi([1,Lt-var.Dt],N_d,1);
    P(3,:) = randi([1,Lt-var.Dt],N_d,1);

end

% Define variable conversions
S_x = 2/(dx*var.Dx);
S_y = 2/(dx*var.Dy);
S_t = (2*Ts_mks)/(dt_mks*var.Dt);

% Create subdomain
var.x = linspace(-1,1,var.Dx);
var.y = linspace(-1,1,var.Dy);
var.t = linspace(-1,1,var.Dt);

% Initialize Target and Library
b = zeros(N_d*N_h(1)*N_h(2)*N_h(3), num_rows*num_cols);
Q = zeros(size(b,1), 3, num_rows*num_cols);

%% FILL TERMS

n_lib = 0;
n_track = 10;
n_partition = 0;

for row = 0:(num_rows-1)
    for col = 0:(num_cols-1)
        U_partition = U_t(row*row_size+1:(row+1)*row_size,...
                          col*col_size+1:(col+1)*col_size,:);
        V_partition = V_t(row*row_size+1:(row+1)*row_size,...
                          col*col_size+1:(col+1)*col_size,:);

        n_partition = n_partition + 1;
        disp(sprintf('Partition #: %d', n_partition));

        for np = 1:N_d

            % disp(sprintf('Domain #: %d', np));
            % Indices for integration domain
            rx = P(1,np):(P(1,np)+var.Dx-1);
            ry = P(2,np):(P(2,np)+var.Dy-1);
            rt = P(3,np):(P(3,np)+var.Dt-1);

            % Velocity fields on integration domain
            %             tic
            U = U_partition(ry,rx,rt);
            V = V_partition(ry,rx,rt);
            %             disp('Loading velocities: '),toc

            for q = 1:N_h(1)
                for r = 1:N_h(2)
                    for s = 1:N_h(3)

                        n_lib = n_lib + 1;

                        if if_track && n_lib == n_track
                            if n_lib < 100
                                disp(['Library Row # : ',num2str(n_lib)])
                                n_track = n_track + 10;
                            elseif n_lib < 1000
                                disp(['Library Row # : ',num2str(n_lib)])
                                n_track = n_track + 100;
                            else
                                disp(['Library Row # : ',num2str(n_lib)])
                                n_track = n_track + 1000;
                            end
                        end

                        % Make wave numbers global to be use in deriveA()
                        var.q = q;
                        var.r = r;
                        var.s = s;

                        % Pre-make derivatives of windowing functions
                        dA101 = derA_m(1,0,1); dA011 = derA_m(0,1,1);
                        dA020 = derA_m(0,2,0); dA200 = derA_m(2,0,0); dA110 = derA_m(1,1,0);
                        dA210 = derA_m(2,1,0); dA030 = derA_m(0,3,0); dA300 = derA_m(3,0,0); dA120 = derA_m(1,2,0);
                        dA100 = derA_m(1,0,0); dA010 = derA_m(0,1,0);
                        % ---> the 3 digits correspond here to derivatives NOT to harmonics

                        % Target
                        B = V.*dA101*S_x*S_t - U.*dA011*S_y*S_t;
                        b(n_lib, n_partition) = trapz(var.x,trapz(var.y,trapz(var.t,B,3)));

                        % Advection Term (incompressible)
                        th1 = U.*V.*(dA020*S_y^2 - dA200*S_x^2) + ...
                              (U.^2 - V.^2).*dA110*S_x*S_y;
                        Q(n_lib, 1, n_partition) = trapz(var.x,trapz(var.y,trapz(var.t,th1,3)));

                        % Laplacian Term
                        th2 = U.*(dA210*S_x^2*S_y + dA030*S_y^3) - ...
                              V.*(dA300*S_x^3 + dA120*S_x*S_y^2);
                        Q(n_lib, 2, n_partition) = trapz(var.x,trapz(var.y,trapz(var.t,th2,3)));

                        % *Simple* Rayleigh Term
                        th3 = V.*dA100*S_x - U.*dA010*S_y;
                        Q(n_lib, 3, n_partition) = trapz(var.x,trapz(var.y,trapz(var.t,th3,3)));


                    end
                end
            end

        end
    end
end



%% REGRESSION

% Parameters

% ksi = SINDy(Theta, b); % sparsify library
ksi = zeros(3, num_cols*num_rows);

for i = 1:(num_cols*num_rows)
    ksi(:,i) = Q(:,:,i) \ b(:,i);
end

beta = ksi(1,:); % non-dim
nu_mks = Us_mks*Ls_mks.*ksi(2,:); %m^2/s
alpha_mks = Us_mks.*ksi(3,:)/Ls_mks; %s^-1

par = [beta;nu_mks;alpha_mks];

% How good was the estimation
% res = norm(b-[Th1,Th2,Th3]*ksi);



%% REMINDERS

disp(' ')
disp(' ')

end
%% --------------------------------------------------------------- SINDy()
function Xi = SINDy (Theta, dXdt)

% compute sparse regression on dX = Theta * Xi
% regression technique used: sequential least squares
% code taken directly from Supporting Info for SINDy Paper

    Xi = Theta \ dXdt;

    ref = mean(abs(dXdt));

    lambda = mean(abs(Theta*Xi - dXdt))/ref
%     lambda = 0.05*mean(abs(dXdt)) % threshold to determine term as "small"

    nrm0 = mean(abs(dXdt))/ref;
    nrm1 = mean(abs(Xi(1)*Theta(:,1)))/ref;
    nrm2 = mean(abs(Xi(2)*Theta(:,2)))/ref;
    nrm3 = mean(abs(Xi(3)*Theta(:,3)))/ref;
%     nrm4 = mean(abs(Xi(4)*Theta(:,4)))/ref;

    R0 = nrm0/lambda;
    R1 = nrm1/lambda;
    R2 = nrm2/lambda;
    R3 = nrm3/lambda;
%     R4 = nrm4/lambda;

    for i = 1:3

      product = zeros(size(Xi)); % product of the coefficient and characteristic size of library function
      [~,w] = size(Theta);
      for p_ind = 1:w
        product(p_ind) = mean(abs(Xi(p_ind)*Theta(:,p_ind)))/ref;
      end

      smallinds = (product < lambda);
      Xi(smallinds) = 0;    % set negligible terms to 0
      for ind = 1:size(dXdt,2)   % perform regression on each vector independently
        biginds = ~smallinds(:,ind);
        Xi(biginds,ind) = Theta(:,biginds) \ dXdt(:,ind);

        lambda = mean(abs(Theta(:,biginds)*Xi(biginds,ind)-dXdt(:,ind)))/ref;

      end
    end

lambda

end
