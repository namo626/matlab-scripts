%% DESCRIPTION
% This script calculates the average and deviation of different
% trials, e.g. domainsN_.mat or resN_.mat in the current directory
% and save them in a struct, along with all the other fields in the
% struct of each file.
function adata = avg_trials(trials, name)

for i = 1:trials
    % load(sprintf('%s%d_0.11_0.33_100.mat',name,i), 'data');
    load(sprintf('freqs%d_2.0y_x.mat',i), 'data');
    betas(i,:) = data.betas;
    nus(i,:) = data.nus;
    alphas(i,:) = data.alphas;
end

%% AVG and STDEV
alpha = 0.065;
beta = 0.826;
nu = 3.23e-6;

err_betas = relError(betas, beta);
err_nus = relError(nus, nu);
err_alphas = relError(alphas, alpha);

avg_betas = mean(err_betas);
avg_nus = mean(err_nus);
avg_alphas = mean(err_alphas);

std_betas = std(err_betas);
std_nus = std(err_nus);
std_alphas = std(err_alphas);

%% Collect results
adata.betas = avg_betas;
adata.nus = avg_nus;
adata.alphas = avg_alphas;

adata.std_betas = std_betas;
adata.std_nus = std_nus;
adata.std_alphas = std_alphas;

adata.N = data.N;
adata.domain = data.domain;
% adata.T = data.T;
adata.weight = data.weight;

adata.fy = data.fy;
adata.fx = data.fx;

if strcmp(name, 'res')
    adata.samplings = data.sampling;
end


end
