%% DESCRIPTION
% Construct the derivatives of a weight of the form (z^2-1)^3*sin(k*2pi*z)
function wc = weight_sin(z, ord, q)

%% INPUTS
% z : vector of the independent variable
% q : frequency of the cosine term
% ord : order of derivative

%% Polynomial part and its derivatives
p0 = h_m(z,0);
p1 = h_m(z,1);
p2 = h_m(z,2);
p3 = h_m(z,3);

%% Differentiate the whole weight
if ord == 0
    wc = p0.*sin(q*2*pi*z);
elseif ord == 1
    wc = p1.*sin(q*2*pi*z) + 2*q*pi*p0.*cos(2*q*pi*z);
elseif ord == 2
    wc = p2.*sin(2*q*pi*z) + 4*q*pi*p1.*cos(2*q*pi*z) - ...
        (2*q*pi)^2*sin(2*q*pi*z).*p0;
elseif ord == 3
    wc = p3.*sin(2*q*pi*z) + 6*q*pi*p2.*cos(2*q*pi*z) - ...
        3*(2*q*pi)^2*p1.*sin(2*q*pi*z) - (2*q*pi)^3*cos(2*q*pi*z).*p0;
else
    error('Max derivative order is 3.');
end