%% DESCRIPTION
% This script repeatedly calls generate_resolution with the same
% configuration and save each one with a different ID. This is done
% for statistical purpose.
function generate_resolutions(trials)

disp(sprintf('%d trials', trials));

tic;
for i = 1:trials
    generate_resolution(i);
end

toc;
