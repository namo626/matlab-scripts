function dA = derA_SCS(derivs, freqs)

global var

dA = combine_weight(@weight_sin, @weight_cos, @weight_S, derivs, freqs);
% dA = combine_weight(@weight_sin, @weight_cos, @weight_sin, derivs, freqs);