function dA = derA_SC(derivs, freqs)

global var

dA = combine_weight(@weight_sin, @weight_cos, derivs, freqs);