function dA = derA_CCS(derivs, freqs)

global var

dA = combine_weight(@weight_cos, @weight_cos, @weight_S, derivs, freqs);
% dA = combine_weight(@weight_cos, @weight_cos, @weight_sin, derivs, freqs);