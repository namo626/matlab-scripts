function dA = derA_SSC(derivs, freqs)

global var

dA = combine_weight(@weight_sin, @weight_sin, @weight_C, derivs, freqs);
% dA = combine_weight(@weight_sin, @weight_sin, @weight_cos, derivs, freqs);