function plotRegions(C)

[dim, regions] = size(C);
x = 1:regions

for i = 1:dim
    scatter(x, C(i,:));
    hold on
end

hold off
