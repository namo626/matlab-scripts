%% DESCRIPTION
% This script generates different combinations of spatial size of
% integration domain
function generate_sizes(N_d, trials, id)

%ymin = 10; ymax = 30;
ymin = 22; ymax = 22;
xmin = 10; xmax = 30;
fry = 0.01*[ymin:2:ymax];
frx = 0.01*[xmin:2:xmax];
%filename = '~/Research/parest-files/20.00mA/state_20.00mA_0300.mat';
filename = '~/Research/parest-files/UV40_19.5mA_4100.mat';
savename = sprintf(['~/Research/parest-files/sizes/' ...
                    'sizes%d_%d_%dy_%d_%dx.mat'], id, ymin,ymax,xmin,xmax);
disp(savename);

betas = zeros(length(fry), length(frx));
nus = zeros(length(fry), length(frx));
alphas = zeros(length(fry), length(frx));

std_betas = zeros(length(fry), length(frx));
std_nus = zeros(length(fry), length(frx));
std_alphas = zeros(length(fry), length(frx));

temp = zeros(trials, 3);

for i = 1:length(fry)
    for j = 1:length(frx)
        disp('');
        disp(sprintf('Domain size: %d%%, %d%%', 100*fry(i), 100*frx(j)));
        for k = 1:trials
            [par,~,~] = IntParEst_fft(filename, N_d, [fry(i),frx(j)], 1,'',1, ...
                                      1);
            temp(k, 1) = par.beta;
            temp(k, 2) = par.nu;
            temp(k, 3) = par.alpha;

        end

        avg = mean(temp,1);
        spread = std(temp, 0, 1);

        betas(i,j) = avg(1);
        nus(i,j) = avg(2);
        alphas(i,j) = avg(3);

        std_betas(i,j) = spread(1);
        std_nus(i,j) = spread(2);
        std_alphas(i,j) = spread(3);
    end
end

data.std_betas = std_betas;
data.std_nus = std_nus;
data.std_alphas = std_alphas;
data.betas = betas;
data.nus = nus;
data.alphas = alphas;
data.N = N_d;
data.weight = par.weight;
data.T = par.T;
data.dx = par.dx;
data.fry = fry;
data.frx = frx;
save(savename, 'data', '-v7.3');


end
