function dA = derA_CS(derivs, freqs)

global var

dA = combine_weight(@weight_cos, @weight_sin, derivs, freqs);