%%%% Multiplicative Windowing Function %%%%
function h = h_m(x,k)

% k : order of derivative (for base, use 0)

%{

h = (x^2-1)^3 = x^6 -3*x^4 + 3*x^2 -1

h' = 6*x*(x^2-1)^2

h'' = 6*(x^2-1)^2 + 24*x^2*(x^2-1)

h''' = 24*x*(x^2-1) + 48*x(x^2-1) + 48*x^3

%}

N = 6; % order of polynomial 

a = [-1 0 3 0 -3 0 1];

h = 0;
for n = k:N
    h = h + a(n+1)*x.^(n-k)*factorial(n)/factorial(n-k);
end


end