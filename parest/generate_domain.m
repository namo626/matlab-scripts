%% DESCRIPTION
% This script collects parameters using different number of samples
% and stores them in a struct

function data = generate_domain(id)

%% INITIALIZE PARAMETERS
fry = 0.1111;
frx = 0.3321;
Dt = 100;
F = @IntParEst_fft;
% func = @derA_SCSC;
filename = '~/Research/parest-files/20.00mA/state_20.00mA_0300.mat';
% samples = [5:9 10:10:90 100:50:450 500:100:1000];
samples = 10:10:100;
savename = sprintf('~/Research/parest-files/domains/fft/domains%d_%.2f_%.2f_%d.mat',id,fry,frx,Dt);
disp(savename)

%% MAIN LOOP

beta    = zeros(1, length(samples));
nu      = zeros(1, length(samples));
alpha   = zeros(1, length(samples));

for i = 1:length(samples)
    [par,~,~] = F(filename,samples(i),[fry,frx],0,'',1,1);

    beta(i) = par.beta;
    nu(i) = par.nu;
    alpha(i) = par.alpha;
end


%% SAVE DATA in the current directory

data.N = samples;
data.betas = beta;
data.nus = nu;
data.alphas = alpha;
data.domain = par.domain;
data.dx = par.dx;
data.T = par.T;
data.weight = par.weight;

save(savename, 'data', '-v7.3');

end