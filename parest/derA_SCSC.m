function dA = derA_CCC(derivs, freqs)

global var

dA = combine_weight(@weight_cos, @weight_cos, @weight_cos, derivs, freqs);