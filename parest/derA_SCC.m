function dA = derA_SCC(derivs, freqs)

global var

dA = combine_weight(@weight_sin, @weight_cos, @weight_C, derivs, freqs);
% dA = combine_weight(@weight_sin, @weight_cos, @weight_cos, derivs, freqs);