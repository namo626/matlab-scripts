function dA = derA_CSS(derivs, freqs)

global var

dA = combine_weight(@weight_cos, @weight_sin, @weight_S, derivs, freqs);
% dA = combine_weight(@weight_cos, @weight_sin, @weight_sin, derivs, freqs);