function generate_frequency(id)

% fy = 2;
% fx = 0.1 * (1:60);
fy = 0.1 * (1:60);
fx = 0.5;
N_d = 20;

betas = zeros(1,length(fx));
nus = zeros(1,length(fx));
alphas = zeros(1,length(fx));

filename = ['~/Research/parest-files/20.00mA/' ...
            'state_20.00mA_0300.mat'];
savename = sprintf('~/Research/parest-files/freqs/freqs%d_y_%.1fx.mat', ...
                   id, fx);
disp(savename);

for i = 1:length(fy)
    [par,Q,b] = IntParEst_fixed(filename,N_d,[0.3,0.3],1,'',1,1,[fy(i), ...
                        fx]);
    betas(i) = par.beta;
    nus(i) = par.nu;
    alphas(i) = par.alpha;

end

data.betas = betas;
data.nus = nus;
data.alphas = alphas;
data.fy = fy;
data.fx = fx;
data.N = N_d;
data.domain = par.domain;
data.weight = par.weight;
data.T = par.T;

save(savename, 'data', '-v7.3');

end