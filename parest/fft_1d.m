%% DESCRIPTION
% Computes the average fft along an axis of a given 3D array. Also
% calculates the frequency axis and the first peak frequency.
function [fpeak, freq, F] = fft_1d(arr, Fs, N, ax)

% Detrending
A = arr - mean(arr, ax);

if ax == 1
    m = 2; n = 3;
elseif ax == 2
    m = 1; n = 3;
else
    m = 1; n = 2;
end

F = mean(mean(abs(fft(A, N, ax)), m), n);
freq = (0:(N-1)) * (Fs/N);
[~, argmax] = max(F);
fpeak = freq(argmax);

end