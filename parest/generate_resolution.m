%% DESCRIPTION
% This script generates parameter estimations for different
% subsampling rates of the same data. Saves the struct in the
% current directory.

function data = generate_resolution(id)
%% INPUT
% id : identifier (for multiple random samples)

%% OUTPUT
% struct { betas, nus, alphas, no of domains, domain size, time
% integration (s), dxs (nondimensional) }

%% INITIALIZE
sampling_rates = 1:10;
func = @derA_daniel;
fry = 0.3;
frx = 0.3;
Dt = 120; % corresponds to 60s
domains = 10;
filename = '~/Research/parest-files/UV40_19.5mA_4100.mat';
savename = sprintf('~/Research/parest-files/resolution/xy/res%d_%.2f_%.2f_%d_%s.mat',id,fry,frx,Dt,func2str(func));
disp(savename);


%% GENERATION
betas = zeros(1, length(sampling_rates));
nus = zeros(1, length(sampling_rates));
alphas = zeros(1, length(sampling_rates));
dxs = zeros(1, length(sampling_rates));

for i = sampling_rates
    [par,~,~] = IntParEst(filename,domains,[1,1,1],[fry,frx,Dt], 1, ...
                          '', i, 1, func);
    betas(i) = par.beta;
    nus(i) = par.nu;
    alphas(i) = par.alpha;
    dxs(i) = par.dx;
end

%% SAVE DATA
data.N = domains;
data.betas = betas;
data.nus = nus;
data.alphas = alphas;
data.dxs = dxs;
data.T = par.T; % time integration in seconds
data.domain = par.domain;
data.sampling = sampling_rates;
data.weight = par.weight;

save(savename, 'data', '-v7.3');

end