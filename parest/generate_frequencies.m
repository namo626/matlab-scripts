function generate_frequencies(trials)

disp(sprintf('%d trials', trials));

tic;
for i = 1:trials
    generate_frequency(i);
end

toc;