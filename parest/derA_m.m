%%% Multiplicative A %%%
function dA = derA_m(L,M,N)
%{

A = (x^2-1)^3*sin(q*pi*x)*(y^2-1)^3*sin(r*pi*y)*sin(s*pi*t)
    -> where the sines can be replaced with cosines as well

%}

global var

% Create subdomain
x = var.x; 
y = var.y;
t = var.t;
[X,Y,T] = meshgrid(x,y,t);

% Normalization Used in All Derivatives
[hx,~,~] = meshgrid(h_m(x,0),y,t);
[hx1,~,~] = meshgrid(h_m(x,1),y,t); % first derivative of h
[hx2,~,~] = meshgrid(h_m(x,2),y,t); % second derivative of h
[hx3,~,~] = meshgrid(h_m(x,3),y,t); % third derivative of h

[~,hy,~] = meshgrid(x,h_m(y,0),t);
[~,hy1,~] = meshgrid(x,h_m(y,1),t); % first derivative of h
[~,hy2,~] = meshgrid(x,h_m(y,2),t); % second derivative of h
[~,hy3,~] = meshgrid(x,h_m(y,3),t); % third derivative of h

if L == 0
    Ax = sin(var.q*pi*X).*hx;
elseif L == 1
    Ax = hx1.*sin(var.q*pi*X) + var.q*pi*hx.*cos(var.q*pi*X);
elseif L == 2 
    Ax = hx2.*sin(var.q*pi*X) + 2*var.q*pi*hx1.*cos(var.q*pi*X) - ...
        (var.q*pi)^2*sin(var.q*pi*X).*hx;
elseif L == 3
    Ax = hx3.*sin(var.q*pi*X) + 3*var.q*pi*hx2.*cos(var.q*pi*X) - ...
        3*(var.q*pi)^2*hx1.*sin(var.q*pi*X) - (var.q*pi)^3*cos(var.q*pi*X).*hx;
end

if M == 0
    Ay = sin(var.r*pi*Y).*hy;
elseif M == 1
    Ay = hy1.*sin(var.r*pi*Y) + (var.r*pi)*cos(var.r*pi*Y).*hy;
elseif M == 2
    Ay = hy2.*sin(var.r*pi*Y) + 2*var.r*pi*hy1.*cos(var.r*pi*Y) - ...
        (var.r*pi)^2*sin(var.r*pi*Y).*hy;
elseif M == 3
    Ay = hy3.*sin(var.r*pi*Y) + 3*var.r*pi*hy2.*cos(var.r*pi*Y) - ...
        3*(var.r*pi)^2*hy1.*sin(var.r*pi*Y) - (var.r*pi)^3*cos(var.r*pi*Y).*hy;
end

if N == 0
    At = sin(var.s*pi*T);
elseif N == 1
    At = var.s*pi*cos(var.s*pi*T);
end

dA = Ax.*Ay.*At;


end